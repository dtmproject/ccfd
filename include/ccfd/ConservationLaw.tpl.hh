/**
 * @file   ConservationLaw.tpl.hh
 * @author Uwe Koecher (UK)
 * @date   2015-02-13, restructuring hierachical model, UK
 * @date   2015-02-10, UK
 *
 * @brief DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/CCFD (Comput. Compressible Fluid Dynamics).    */
/*                                                                            */
/*  DTM++/CCFD is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/CCFD is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/CCFD.   If not, see <http://www.gnu.org/licenses/>.      */

/* ORIGIN of this code was (partly) deal.II/example/step-33/step-33.cc:
 * ---------------------------------------------------------------------
 *
 * Copyright (C) 2007 - 2015 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 *
 * Author: David Neckels, Boulder, Colorado, 2007, 2008
 */

////////////////////////////////////////////////////////////////////////////////
// TODO ISSUE LIST:
// 
// Major Prio:
// - TODO  Create "Grid"-class (DTM++ style)
// - FIXME Make this a base class for (hyperbolic) ConservationLaw Problems
//         -> exclude everything with "EulerEquations"
// - FIXME exclude Parameter Input Handling (seperate class)
// - FIXME exclude output_results => DTM++/output class for ccfd/eulerEq
//         -> allow parallel file-IO for MPI usage
// 
// Minor Prio:
// - TODO Check if dealii::Vector can be replaced with MPI-ready Trilinos::MPI::Vector
// - TODO exclude assemblies into seperate files (classes) / using DTM++ MPI+X Assemblers
// - TODO exclude Time Marching Iteration (fractional-step-theta)
//   * exclude linear system solver "solve()" (iterative/direct)
//     => use DTM++.core solver(s)
//   * exclude Newton solver (if possible and feasible)
//     => create DTM++.core/Newton solver
////////////////////////////////////////////////////////////////////////////////

#ifndef __ConservationLaw_tpl_hh
#define __ConservationLaw_tpl_hh

// PROJECT includes
#include <ccfd/Parameters.tpl.hh>

// DEAL.II includes
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/types.h>

#include <deal.II/base/function.h>

#include <deal.II/dofs/dof_handler.h>

#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/mapping.h>
#include <deal.II/fe/mapping_q.h>

#include <deal.II/grid/tria.h>

#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/vector.h>

// TRILINOS includes
#include <Sacado.hpp>

// C++ includes
#include <array>
#include <utility>
#include <vector>


namespace ccfd {
namespace euler {

////////////////////////////////////////////////////////////////////////////////
// Declare some typename ALIASES (C++.11 only style)
// additionally, std::array< > is only feasible for C++.11

using SolutionComponentType = Sacado::Fad::DFad<double>;

template<int n_components>
using SolutionType = std::array<SolutionComponentType, n_components>;

template<int n_components, int dim>
using SolutionJacobianType = std::array<std::array<SolutionComponentType, dim>, n_components>;


using FluxFunctionType = Sacado::Fad::DFad<double>;

template<int n_components>
using FluxFunctionVectorType = std::array<FluxFunctionType, n_components>;

template<int n_components, int dim>
using FluxMatrixType = std::array<std::array<FluxFunctionType, dim>, n_components>;



template<int n_components>
using ForcingVectorType = std::array<Sacado::Fad::DFad<double>, n_components>;

////////////////////////////////////////////////////////////////////////////////


/** Base Class ConservationLaw: Implements 1st order hyperbolic conservation law. */
template<int dim, int n_components>
class ConservationLaw {
public:
	ConservationLaw();
	
	virtual ~ConservationLaw() = default;
	
	/** Run Simulation. Implements the Newton solver and the time stepping integrator */
	virtual void run();
	
	virtual void set_input_parameters(dealii::ParameterHandler &parameters);

protected:
	/** Interface function for computing the flux matrix
	 * \f$ \boldsymbol F(\boldsymbol u) \f$.
	 * 
	 * \note This function must be implemented in a specialised class.
	 */
	virtual
	void
	flux_matrix(
		const SolutionType<n_components> &u,
		FluxMatrixType<n_components,dim> &FluxMatrix) const {
		AssertThrow(false, dealii::ExcNotImplemented());
	}
	
	/** Interface function for computing the forcing vector
	 * \f$ \boldsymbol f(\boldsymbol u) \f$.
	 * 
	 * \note This function must be implemented in a specialised class.
	 */
	virtual
	void
	forcing_vector(
		const SolutionType<n_components> &u,
		ForcingVectorType<n_components> &ForcingVector) const {
		AssertThrow(false, dealii::ExcNotImplemented());
	}
	
	/** Numerical normal flux: Lax-Friedrich's flux with stabilization.
	 * 
	 * \param[in] u_plus \f$ \boldsymbol u^+ \f$ .
	 * \param[in] u_minus \f$ \boldsymbol u^- \f$ .
	 * \param[in] normal Normal vector \f$ \boldsymbol n \f$ .
	 * \param[in] alpha Stabilization parameter \f$ \alpha \f$ .
	 * \param[in,out] normal_flux n_components array of flux functions.
	 * 
	 * We use a numerical flux function to enforce the boundary conditions and
	 * to enforce the continuity of the solution at hanging nodes in a weak sense
	 * on interior element boundaries.
	 * 
	 * This function implements the Lax-Friedrich's flux with the stabilization
	 * parameter \f$ \alpha \f$.
	 * 
	 * More precisely, the Lax-Friedrich's flux for an arbitrary vector-valued function
	 * \f$ \boldsymbol H :=
	 * \boldsymbol H\big( \boldsymbol u^+, \boldsymbol u^-, \boldsymbol n \big)\f$
	 * is given as
	 * \f[ \boldsymbol h\big( \boldsymbol u^+, \boldsymbol u^-, \boldsymbol n \big) =
	 *     \frac{1}{2} \Big( \boldsymbol F\big( \boldsymbol u^+ \big) \, \boldsymbol n +
	 *     \boldsymbol F\big( \boldsymbol u^- \big) \, \boldsymbol n \Big) +
	 *     \frac{1}{2} \, \alpha\, \big( \boldsymbol u^+ - \boldsymbol u^- \big)\,,
	 * \f]
	 * with the flux matrix \f$ \boldsymbol F(\cdot) \f$ and the stabilization
	 * parameter \f$ \alpha \f$ .
	 * 
	 * This function calls the member function
	 * ConservationLaw< dim, n_components >::flux_matrix.
	 */
	void
	numerical_normal_flux(
		const SolutionType<n_components> &u_plus,
		const SolutionType<n_components> &u_minus,
		const dealii::Tensor<1,dim> normal,
		const double alpha,
		FluxFunctionVectorType<n_components> &normal_flux
	);
	
	
	void setup_system();
	
	
	////////////////////////////////////////////////////////////////////////////
	// TODO: Put the concrete assemblies into separate classes!
	void assemble_system();
	
	void assemble_cell_term(
		const dealii::FEValues<dim> &fe_values,
		const std::vector<dealii::types::global_dof_index> &dofs
	);
	
	/** assembles face terms
	 * Assemble Boundary (external) and Internal face terms.
	 * \param [in] face_no Number of the face corresponding to the cell
	 * \param [in] fe_face_values FEFaceValues object of the face
	 * \param [in] fe_face_values_neighbor (Interior Face ONLY) FEFaceValues object of the neighboring face
	 * \param [in] dofs Vector of global DoF indices of the cell
	 * \param [in] dofs_neighbor (Interior Face ONLY) Vector of global DoF indices of the neighbor cell
	 * \param [in] interior_face True indicates an interior face.
	 * \param [in] boundary_id Boundary identifier for external faces.
	 * \param [in] face_diameter Face diameter (h_F).
	 */
	void assemble_face_term(
		const unsigned int face_no,
		const dealii::FEFaceValuesBase<dim> &fe_face_values,
		const dealii::FEFaceValuesBase<dim> &fe_face_values_neighbor,
		const std::vector<dealii::types::global_dof_index> &dofs,
		const std::vector<dealii::types::global_dof_index> &dofs_neighbor,
		const bool interior_face,
		const unsigned int &boundary_id,
		const double face_diameter
	);
	
	
	/** Interface function to enforce boundary conditions for \f$ \boldsymbol u^- \f$.
	 * 
	 * \param [in] u_plus Vector of all interior solution variables \f$ \boldsymbol u^+ \f$.
	 * \param [in] boundary_indicator Boundary indicator number.
	 * 
	 * \param [in] quadrature_points Vector of all face quadrature points.
	 * \param [in] normal_vectors Vector of all normal vectors in the quadrature points.
	 * 
	 * \param [in,out] u_minus Vector of all exterior solution variables \f$ \boldsymbol u^- \f$.
	 * 
	 * \note This function must be implemented in a specialised class.
	 */
	virtual
	void
	enforce_boundary_conditions(
		const std::vector< SolutionType<n_components> > &u_plus,
		const unsigned int &boundary_indicator,
		const std::vector< dealii::Point<dim> > &quadrature_points,
		const std::vector< dealii::Point<dim> > &normal_vectors,
		std::vector< SolutionType<n_components> > &u_minus
	) const {
		AssertThrow(false, dealii::ExcNotImplemented());
	}
	
	
	virtual std::pair<unsigned int, double> solve(
		dealii::Vector<double> &solution
	);
	
	/** Interface function for computing local indicators for spatial mesh adaptivity.
	 * 
	 * \note This function must be implemented in a specialised class.
	 */
	virtual
	void
	compute_refinement_indicators(
		const dealii::DoFHandler<dim> &dof,
		const dealii::Mapping<dim> &mapping,
		const dealii::Vector<double> &solution,
		dealii::Vector<double> &refinement_indicators
	) const {
		AssertThrow(false, dealii::ExcNotImplemented());
	}
	
	virtual void refine_grid(const dealii::Vector<double> &indicator);
	
	
	/** Output simulation results.
	 * 
	 * \note This function must be implemented in a specialised class.
	 */
	virtual void output_results() {
		AssertThrow(false, dealii::ExcNotImplemented());
	}
	
	
	////////////////////////////////////////////////////////////////////////////
	// TODO: Create DTM++ styled Grid-class for computational mesh handlers
	//
	
	// NOTE: We define a mapping object to be used throughout the program
	//       when assembling terms
	//       (we will hand it to every FEValues and FEFaceValues object).
	//       
	//       The mapping we use is just the standard $Q_1$ mapping
	//       -- nothing fancy, in other words --
	//       but declaring one here and using it throughout the program will
	//       make it simpler later on to change it
	//       if that should become necessary.
	//       
	//       HERE: This is, in fact, rather pertinent: it is
	//       known that for TRANSSONIC simulations with the EULER EQUATIONS,
	//       computations DO NOT CONVERGE (even as $h\rightarrow 0$)
	//       IF the BOUNDARY APPROXIMATION is not of sufficiently high order.
	
	const unsigned int p;
	
	dealii::Triangulation<dim>   triangulation; // TODO: parallel distributed
	const dealii::MappingQ<dim>  mapping;
	
	const dealii::FESystem<dim>  fe;
	std::shared_ptr< dealii::DoFHandler<dim> > dof; // TODO: parallel distributed
	
	const dealii::QGauss<dim>    quadrature;
	const dealii::QGauss<dim-1>  face_quadrature;
	
	//
	////////////////////////////////////////////////////////////////////////////
	
	////////////////////////////////////////////////////////////////////////////
	// Global vectors & system matrix
	//
	
	// A number of data vectors that correspond to the solution of
	// the previous time step (<code>old_solution</code>), the best guess of
	// the current solution (<code>current_solution</code>; we say
	// <i>guess</i> because the Newton iteration to compute it may not have
	// converged yet, whereas <code>old_solution</code> refers to the fully
	// converged final result of the previous time step), and a predictor for
	// the solution at the next time step, computed by extrapolating the
	// current and previous solution one time step into the future:
	
	dealii::Vector<double>       old_solution;
	dealii::Vector<double>       current_solution;
	double                       time;
	dealii::Vector<double>       predictor;
	
	dealii::Vector<double>       right_hand_side;
	
	// This final set of member variables (except for the object holding all
	// run-time parameters at the very bottom and a screen output stream that
	// only prints something if verbose output has been requested) deals with
	// the interface we have in this program to the Trilinos library that
	// provides us with linear solvers. Similarly to including PETSc matrices
	// in step-17, step-18, and step-19, all we need to do is to create a
	// Trilinos sparse matrix instead of the standard deal.II class. The
	// system matrix is used for the Jacobian in each Newton step. Since we do
	// not intend to run this program in parallel (which wouldn't be too hard
	// with Trilinos data structures, though), we don't have to think about
	// anything else like distributing the degrees of freedom.
	
	dealii::TrilinosWrappers::SparseMatrix system_matrix;
	
	//
	////////////////////////////////////////////////////////////////////////////
	
	
	/** Initial value function with n_components */
	std::shared_ptr< dealii::Function<dim> > u_0;
	
	
	dealii::ConditionalOStream     verbose_cout;
	dealii::ConditionalOStream     pcout; // output of the program
	
	unsigned int MyPID;
	
	// 
	const double param_output_step=0.01;
	const double tol_newton_solver=1e-10;
	const unsigned int max_iter_newton_solver=10;
	//
	
	
	// input parameters
	
	double tau;
	double tau_min;
	double tau_max;
	double theta;
	double t0;
	double T;
	
	std::string mesh_filename;
	bool h_adaptivity_enabled;
	
	bool shock_caputuring_enabled;
	unsigned int shock_levels;
	double shock_value;
	
	double streamline_diffusion_power;
	
	bool input_parameters_set = false;
};

}} // namespace

#endif

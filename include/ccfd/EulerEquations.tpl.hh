/**
 * @file   EulerEquations.tpl.hh
 * @author Uwe Koecher (UK)
 * @date   2015-02-13, introducing hierachical model, UK
 * @date   2015-02-10, UK
 *
 * @brief DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/CCFD (Comput. Compressible Fluid Dynamics).    */
/*                                                                            */
/*  DTM++/CCFD is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/CCFD is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/CCFD.   If not, see <http://www.gnu.org/licenses/>.      */

/* ORIGIN of this code was (partly) deal.II/example/step-33/step-33.cc:
 * ---------------------------------------------------------------------
 *
 * Copyright (C) 2007 - 2015 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 *
 * Author: David Neckels, Boulder, Colorado, 2007, 2008
 */

#ifndef __EulerEquations_tpl_hh
#define __EulerEquations_tpl_hh

// PROJECT includes
#include <DTM++/core/io/DataOutput.tpl.hh>
#include <ccfd/ConservationLaw.tpl.hh>

// DEAL.II includes
#include <deal.II/base/conditional_ostream.h>
#include <deal.II/base/function.h>
#include <deal.II/base/quadrature.h>
#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/tensor_function.h>
#include <deal.II/base/types.h>

#include <deal.II/dofs/dof_handler.h>

#include <deal.II/fe/fe_values.h>
#include <deal.II/fe/fe_system.h>
#include <deal.II/fe/mapping.h>
#include <deal.II/fe/mapping_q1.h>

#include <deal.II/grid/tria.h>

#include <deal.II/lac/trilinos_sparse_matrix.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/trilinos_vector.h>

#include <deal.II/numerics/data_component_interpretation.h>
#include <deal.II/numerics/data_postprocessor.h>

// C++ includes
#include <memory>
#include <utility>
#include <vector>

namespace ccfd {
namespace euler {


/** EulerEquations: Implements non-linear system of first-order hyperbolic conservation laws.
 * 
 * <h1>Euler equations of gas dynamics</h1>
 * <b>A simplified prototype model for the movement of a compressible inviscid
 * fluid is described by the Euler equations of gas dynamics.</b>
 * 
 * The Euler equations belong to the class of non-linear first-order hyperbolic
 * partial differential equations in the space-time domain.
 * Therefore, we implement them here as specialization of the
 * dimension-less base class ConservationLaw which is intended as
 * interface class for the implementation of
 * (general) first-order hyperbolic conservation laws.
 * More precisely, the Euler equations are a simplified case of the general
 * compressible Navier-Stokes equations in which the viscosity terms and
 * the thermal conductivity terms vanish.
 * 
 * For completeness, the following line-wise interpreted system of the
 * Euler equations is build of
 * 
 * - the conservation law of the mass by the equation \f$ (1) \f$,
 * - the conservation law of the momentum by the equation(s) \f$ (2)-(d+1) \f$ and
 * - the conservation law of the total energy by the equation \f$ (d+2) \f$
 * 
 * for a spatial dimension \f$ d \f$ with \f$ d \in \{ 1,2,3 \} \f$.
 * 
 * 
 * <b>The dimension-less Euler equations of gas dynamics</b>
 * for the vector-valued solution variable \f$ \boldsymbol u \f$,
 * \f[
 *   \boldsymbol u = \big( \rho, \rho v_1, \dots, \rho v_d, E \big)^T \,,
 * \f]
 * are given by
 * \f[
 *   \partial_t \boldsymbol u + \nabla \cdot \boldsymbol F(\boldsymbol u) =
 *   \boldsymbol f(\boldsymbol u) \qquad
 *   \text{in}\quad \Omega \times I\,,
 * \f]
 * in \f$ \Omega \subset \mathbb R^d \f$ and \f$ I := (0,T) \f$,<br>
 * equipped with an appropriate set of boundary conditions from
 * EulerEquations< dim >::boundary_id <br>
 * and initial conditions
 * \f[
 *   \boldsymbol u_0 = \big( \rho_0, \rho_0 \boldsymbol v_0^T, E_0 )^T
 * \f]
 * generated from \f$ \rho_0 \f$, \f$ \boldsymbol v_0 \f$ and \f$ p_0 \f$.
 * 
 * 
 * The vector-valued solution variable \f$ \boldsymbol u \f$ consists of
 * a composition of independent solution variables which are denoted as
 * - the scalar-valued gas mass <b>density</b> \f$ \rho \f$,
 * - the vector-valued gas flow <b>velocity</b>
 *   \f$ \boldsymbol v = \big( v_1, \dots, v_d \big)^T \f$ and
 * - the scalar-valued <b>total energy density</b> (total energy per unit volume)
 *   \f$ E \f$ of the gas.
 * 
 * For completeness and being precisely,
 * the solution variable \f$ \boldsymbol u \f$ contains the
 * vector-valued <b>momentum</b> variable \f$ \boldsymbol m := \rho \boldsymbol v \f$
 * which is considered further as the composition of the solution variables
 * \f$ \rho \f$ and \f$ \boldsymbol v \f$ in the following way
 * \f[ \rho \boldsymbol v = \big( \rho v_1, \dots, \rho v_d \big)^T
 *   =: (m_1, \dots, m_d)^T =: \boldsymbol m\,.
 * \f]
 * 
 * For the Euler equations,
 * the <b>flux matrix</b> \f$ \boldsymbol F(\boldsymbol u) \f$,
 * i.e. a matrix of flux functions,
 * is here exemplarily given for \f$ d=3 \f$ as
 * \f[ \boldsymbol F(\boldsymbol u) :=
 * \begin{pmatrix}
 *   \rho v_1 & \rho v_2 & \rho v_3 \\
 *   \rho v_1^2 + p & \rho v_2 v_1 & \rho v_3 v_1 \\
 *   \rho v_1 v_2 & \rho v_2^2 + p & \rho v_3 v_2 \\
 *   \rho v_1 v_3 & \rho v_2 v_3 & \rho v_3^2 + p \\
 *   (E + p) v_1 & (E + p) v_2 & (E + p) v_3
 * \end{pmatrix} \,,
 * \f]
 * which is implemented in EulerEquations< dim >::flux_matrix.
 * 
 * In the flux matrix \f$ \boldsymbol F(\boldsymbol u) \f$,
 * we denote by \f$ p \f$ the scalar-valued <b>pressure</b>
 * defined by the relation
 * \f[ p :=
 * \big( \gamma - 1 \big)\,
 * \bigg( E - \frac{1}{2}\, \rho\, | \boldsymbol v |^2 \bigg)\,,
 * \f]
 * where we denote by \f$ \gamma \f$ the ratio of specific heats of the gas.<br>
 * The pressure relation is implemented in EulerEquations< dim >::pressure.
 * 
 * 
 * We put here \f$ \gamma = 1.4 \f$ as ratio of specific heats for diatomic gases;
 * e.g. for the mainly diatomic gas composition of the atmospheric air,
 * which is consisting mainly of
 * nitrogen \f$ (N_2) \f$ with an approx. amount of 78 Vol.-% and
 * oxygen \f$ (O_2) \f$ with an approx. amount of 20 Vol.-%,
 * whereby the volume-percentage amounts are given exemplarily for an usual
 * atmosphere pressure at the sea-level.
 * 
 * For simplicity, we choose here to apply only the effects of gravity as the
 * right-hand side acceleration for the
 * <b>forcing vector</b> \f$ \boldsymbol f( \boldsymbol u ) \f$ defined by
 * \f[ \boldsymbol f(\boldsymbol u) :=
 * \begin{pmatrix}
 *   0 \\
 *   \rho\, \boldsymbol g \\
 *   \boldsymbol g \cdot \rho \boldsymbol v
 * \end{pmatrix} \,,
 * \f]
 * where we denote by \f$ \boldsymbol g \in \mathbb{R}^d \f$
 * a \f$ d \f$-dimensional <b>gravity vector</b>.<br>
 * The right-hand side forcing vector is implemented in
 * EulerEquations< dim >::forcing_vector.
 * 
 */
template<int dim, int n_components>
class EulerEquations : public ConservationLaw<dim,n_components> {
public:
	
	EulerEquations();
	
	virtual ~EulerEquations() = default;
	
	virtual void set_input_parameters(dealii::ParameterHandler &parameters);
	
	/** Returns a copy of the member component_names . */
	virtual
	std::vector<std::string>
	get_component_names() const;
	
	/** Returns a copy of the member dci . */
	virtual
	std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation>
	get_dci() const;

protected:
	
	/** Compute the flux matrix \f$ \boldsymbol F(\boldsymbol u) \f$.
	 * 
	 * @param[in] u Input vector of the solution \f$ \boldsymbol u \f$.
	 * @param[in,out] FluxMatrix NumberType[][] array of flux functions.
	 * 
	 * The <b>flux matrix</b>
	 * \f$ \boldsymbol F(\boldsymbol u) := \boldsymbol F^d(\boldsymbol u) \f$,
	 * i.e. a matrix of flux functions, is here completely implemented in a
	 * dimension-independed way. However, for better readability,
	 * we give here \f$ \boldsymbol F^d(\boldsymbol u) \f$ for \f$ d=1,2,3 \f$
	 * explicitly as
	 * \f[\boldsymbol F^1(\boldsymbol u) :=
	 * \begin{pmatrix}
	 *   \rho v_1 \\
	 *   \rho v_1^2 + p \\
	 *   (\rho E + p) v_1
	 * \end{pmatrix} \,,\qquad
	 * 
	 * \boldsymbol F^2(\boldsymbol u) :=
	 * \begin{pmatrix}
	 *   \rho v_1 & \rho v_2 \\
	 *   \rho v_1^2 + p & \rho v_2 v_1 \\
	 *   \rho v_1 v_2 & \rho v_2^2 + p \\
	 *   (\rho E + p) v_1 & (\rho E + p) v_2
	 * \end{pmatrix} \,,\qquad
	 * 
	 * \boldsymbol F^3(\boldsymbol u) :=
	 * \begin{pmatrix}
	 *   \rho v_1 & \rho v_2 & \rho v_3 \\
	 *   \rho v_1^2 + p & \rho v_2 v_1 & \rho v_3 v_1 \\
	 *   \rho v_1 v_2 & \rho v_2^2 + p & \rho v_3 v_2 \\
	 *   \rho v_1 v_3 & \rho v_2 v_3 & \rho v_3^2 + p \\
	 *   (\rho E + p) v_1 & (\rho E + p) v_2 & (\rho E + p) v_3
	 * \end{pmatrix} \,.
	 * \f]
	 * 
	 * In that flux matrix, we denote by \f$ p \f$ the scalar-valued <b>pressure</b>
	 * defined by the relation
	 * \f[ p =
	 *   \big( \gamma - 1 \big)\,
	 *   \big( \rho E - E_{\text{kin}}(\boldsymbol u) \big)\,,
	 * \f]
	 * where we denote by \f$ \gamma \f$ the ratio of specific heats of the fluid
	 * and by \f$ E_{\text{kin}} \f$ the kinetic energy.
	 * 
	 * This function calls the member function EulerEquations< dim >::pressure.
	 * 
	 */
	virtual
	void
	flux_matrix(
		const SolutionType<n_components> &u,
		FluxMatrixType<n_components,dim> &FluxMatrix
	) const;
	
	
	/** Compute the forcing vector \f$ \boldsymbol f(\boldsymbol u) \f$.
	 * 
	 * @param[in] u Input vector of the solution \f$ \boldsymbol u \f$.
	 * @param[in,out] ForcingVector NumberType[] array of flux functions.
	 * 
	 * The <b>forcing vector</b> \f$ \boldsymbol f( \boldsymbol u ) \f$ is here
	 * implemented, for simplicity, to apply only the effects of gravity as
	 * right-hand side acceleration, as given by
	 * \f$ \boldsymbol f(\boldsymbol u) := \boldsymbol f^d(\boldsymbol u) \f$,
	 * \f$ d=1,2,3 \f$, with
	 * \f[ \boldsymbol f^1(\boldsymbol u) =
	 * \begin{pmatrix}
	 *   0 \\
	 *   g_1\, \rho \\
	 *   \boldsymbol g \cdot \rho \boldsymbol v
	 * \end{pmatrix} \,,\qquad
	 * 
	 * \boldsymbol f^2(\boldsymbol u) =
	 * \begin{pmatrix}
	 *   0 \\
	 *   g_1\, \rho \\
	 *   g_2\, \rho \\
	 *   \boldsymbol g \cdot \rho \boldsymbol v
	 * \end{pmatrix} \,,\qquad
	 * 
	 * \boldsymbol f^3(\boldsymbol u) =
	 * \begin{pmatrix}
	 *   0 \\
	 *   g_1\, \rho \\
	 *   g_2\, \rho \\
	 *   g_3\, \rho \\
	 *   \boldsymbol g \cdot \rho \boldsymbol v
	 * \end{pmatrix} \,,
	 * \f]
	 * where we denote by
	 * \f$ \boldsymbol g = (g_1, \dots, g_d)^T \f$ the \f$ d \f$-dimensional
	 * <b>gravity vector</b> given by EulerEquations< dim >::gravity_vector.
	 * 
	 */
	virtual
	void
	forcing_vector(
		const SolutionType<n_components> &u,
		ForcingVectorType<n_components> &ForcingVector
	) const;
	
	
	/** Enforce boundary conditions for \f$ \boldsymbol u^- \f$.
	 * 
	 * \param [in] u_plus Vector of all interior solution variables \f$ \boldsymbol u^+ \f$.
	 * \param [in] boundary_indicator Boundary indicator number.
	 * 
	 * \param [in] quadrature_points Vector of all face quadrature points.
	 * \param [in] normal_vectors Vector of all normal vectors in the quadrature points.
	 * 
	 * \param [in,out] u_minus Vector of all exterior solution variables \f$ \boldsymbol u^- \f$.
	 * 
	 * Considering further the documentation of EulerEquations< dim >::boundary_id
	 * for a details.
	 */
	virtual
	void
	enforce_boundary_conditions(
		const std::vector< SolutionType<n_components> > &u_plus,
		const unsigned int &boundary_indicator,
		const std::vector< dealii::Point<dim> > &quadrature_points,
		const std::vector< dealii::Point<dim> > &normal_vectors,
		std::vector< SolutionType<n_components> > &u_minus
	) const;
	
	
	/** Compute indicators for spatial mesh adaptivity.
	 * 
	 * Here, we use the gradient of the density \f$ \rho \f$ out of the complete
	 * solution \f$ \boldsymbol u \f$ to compute a local indicator \f$ \eta_K \f$
	 * \f[
	 *   \eta_K(t) = \log\left( 1 + \| \nabla \rho( \boldsymbol x_K, t) \| \right)
	 * \f]
	 * where \f$ \boldsymbol x_K \f$ is in the geometric center of the mesh
	 * cell \f$ K \f$ and for a fixed (constant) time value \f$ t \f$.
	 */
	virtual
	void
	compute_refinement_indicators(
		const dealii::DoFHandler<dim> &dof,
		const dealii::Mapping<dim> &mapping,
		const dealii::Vector<double> &solution,
		dealii::Vector<double> &refinement_indicators
	) const;
	
	
	virtual void output_results();
	
private:
	
	/** Compute the kinetic energy \f$ E_{\text{kin}} \f$ from \f$ \boldsymbol u \f$.
	 * @param[in] u Input vector of the solution \f$ \boldsymbol u \f$.
	 * @return Returns the value of the kinetic energy.
	 * 
	 * The kinetic energy \f$ E_{\text{kin}} \f$ of the solution
	 * \f$ \boldsymbol u = \big( \rho, m_1, \dots, m_d, E \big)^T \f$
	 * is derived as
	 * \f[
	 *   E_{\text{kin}} = \frac{1}{2}\, \rho\, | \boldsymbol v |^2\,.
	 * \f]
	 * Since the solution vector \f$ \boldsymbol u \f$ contains the momentum
	 * solution components and not the velocity solution \f$ \boldsymbol v \f$,
	 * we compute here
	 * \f[
	 *   E_{\text{kin}} =
	 *     \frac{1}{2\, \rho}\,
	 *     \sum_{k=1}^{d} m_k^2 \,,\qquad
	 *   m_k = \rho v_k,\quad k=1,\dots,d\,.
	 * \f]
	 * 
	 */
	SolutionComponentType
	kinetic_energy(const SolutionType<n_components> &u) const;
	
	
	/** Compute the pressure \f$ p \f$ from \f$ \boldsymbol u \f$.
	 * 
	 * @param[in] u Input vector of the solution \f$ \boldsymbol u \f$.
	 * @return Returns the value of the pressure \f$ p \f$.
	 * 
	 * The pressure \f$ p \f$ of the solution
	 * \f$ \boldsymbol u = \big( \rho, m_1, \dots, m_d, E \big)^T \f$
	 * is derived as
	 * \f[ p =
	 *   \big( \gamma - 1 \big)\,
	 *   \big( E - E_{\text{kin}}(\boldsymbol u) \big)\,,
	 * \f]
	 * where we denote by \f$ \gamma \f$ the ratio of specific heats of the fluid,
	 * given by EulerEquations< dim >::gamma,<br>
	 * and by \f$ E_{\text{kin}} \f$ the kinetic energy which is computed by
	 * calling EulerEquations< dim >::kinetic_energy.
	 * 
	 */
	SolutionComponentType
	pressure(const SolutionType<n_components> &u) const;
	
	
	/** Boundary indicator enumeration class.
	 * 
	 * We define the boundary_id enumeration
	 * for prescribing or extrapolating a boundary condition for<br>
	 * the solution \f$ \boldsymbol u^- \f$ on a part of the boundary
	 * \f$ \partial \Omega \f$.
	 * 
	 * In the following, we use
	 * - \f$ \rho_D = \rho_D(\boldsymbol x, \cdot) \f$,
	 * - \f$ \boldsymbol v_D = \big( v_1^D, \dots, v_d^D \big)^T
	 * = \boldsymbol v_D(\boldsymbol x, \cdot) \f$ and
	 * - \f$ p_D = p_D(\boldsymbol x, \cdot) \f$
	 * 
	 * as precribed functions which can be evaluated on \f$ \partial \Omega \f$.
	 * 
	 * The boundary types and values are applied by calling
	 * EulerEquations< dim >::enforce_boundary_conditions.
	 */
	enum class boundary_id : unsigned int {
		/** Inflow (supersonic) boundary condition.
		 * Set
		 * \f[ \boldsymbol u^- :=
		 * \begin{pmatrix}
		 *   \rho_D \\
		 *   \rho_D v_1^D \\
		 *   \vdots \\
		 *   \rho_D v_d^D \\
		 *   E_D
		 * \end{pmatrix}\,,\qquad
		 * E_D := \frac{p_D}{(\gamma - 1)} +
		 *   \frac{1}{2}\, \rho_D\, | \boldsymbol v_D |^2\,.
		 * \f]
		 */
		inflow_supersonic = 1,
		
		/** Inflow (subsonic) boundary condition.
		 * Set
		 * \f[ \boldsymbol u^- :=
		 * \begin{pmatrix}
		 *   \rho_D \\
		 *   \rho_D v_1^D \\
		 *   \vdots \\
		 *   \rho_D v_d^D \\
		 *   E_D^+
		 * \end{pmatrix}\,,\qquad
		 * E_D^+ := \frac{p^+}{(\gamma - 1)} +
		 *   \frac{1}{2}\, \rho_D\, | \boldsymbol v_D |^2\,.
		 * \f]
		 */
		inflow_subsonic = 2,
		
		/** Outflow (supersonic) boundary condition.
		 * Set \f$ \boldsymbol u^- := \boldsymbol u^+ \f$.
		 */
		outflow_supersonic = 3,
		
		/** Outflow (subsonic) boundary condition.
		 * Set
		 * \f[ \boldsymbol u^- :=
		 * \begin{pmatrix}
		 *   \rho^+ \\
		 *   m_1^+ \\
		 *   \vdots \\
		 *   m_d^+ \\
		 *   E_{p_D}^+
		 * \end{pmatrix}\,,\qquad
		 * E_{p_D}^+ := \frac{p_D}{(\gamma - 1)} +
		 *   \frac{1}{2}\, \rho^+\, | \boldsymbol v^+ |^2
		 * \f]
		 * with the prescribed pressure \f$ p_D \f$.
		 */
		outflow_subsonic = 4,
		
		/** Solid impermeable boundary condition.
		 * Set \f$ \rho^- := \rho^+ \f$,
		 * the momentum components of \f$ \boldsymbol u^- \f$, such that
		 * \f[
		 *   0 = \big( \boldsymbol v^- + \boldsymbol v^+ \big) \cdot \boldsymbol n
		 * \f]
		 * is fulfilled and \f$ E^- := E^+ \f$.
		 */
		solid_impermeable = 5
	}; // boundary_id
	
	
	/** Component field number of density component. */
	const unsigned int density_component = 0;
	
	/** Component field number of first momentum component. */
	const unsigned int first_momentum_component = 1;
	
	/** Component field number of energy component. */
	const unsigned int energy_component = dim+1;
	
	/** Component names of solution variables. */
	std::vector<std::string> component_names;
	
	/** Data component interpretation of solution variables. */
	std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation> dci;
	
	std::shared_ptr< dealii::DataPostprocessor<dim> > data_postprocessor;
	
	std::shared_ptr< DTM::core::DataOutput< dim,dealii::Vector<double> > >
		data_out_postprocessed_solution;
	
	/** Ratio of specific heats \f$ \gamma \f$. */
	const double gamma;
	const double gamma_minus_one;
	
	/** Gravity vector */
	dealii::Point<dim> gravity_vector;
	
	
	/** Prescribed boundary value function \f$ \rho_D \f$ */
	std::shared_ptr< dealii::Function<dim> > rho_D;
	
	/** Prescribed boundary value function \f$ \boldsymbol v_D \f$ */
	std::shared_ptr< dealii::TensorFunction<1,dim> > v_D;
	
	/** Prescribed boundary value function \f$ p_D \f$ */
	std::shared_ptr< dealii::Function<dim> > p_D;
	
}; // class

}} // namespace

#endif

/**
 * @file   EulerEquationsDataPostprocessor.tpl.hh
 * @author Uwe Koecher (UK)
 * @date   2015-02-24, UK
 *
 * @brief DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/CCFD (Comput. Compressible Fluid Dynamics).    */
/*                                                                            */
/*  DTM++/CCFD is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/CCFD is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/CCFD.   If not, see <http://www.gnu.org/licenses/>.      */

#ifndef __EulerEquationsDataPostprocessor_tpl_hh
#define __EulerEquationsDataPostprocessor_tpl_hh

// PROJECT includes

// DEAL.II includes
#include <deal.II/base/point.h>
#include <deal.II/base/tensor.h>

#include <deal.II/lac/vector.h>

#include <deal.II/numerics/data_postprocessor.h>


// C++ includes
#include <string>
#include <vector>


namespace ccfd {
namespace euler {

enum OutputQuantities : unsigned int {
	density = 1,
	momentum = 2,
	velocity = 4,
	pressure = 8,
	total_energy_volume = 16,
	total_energy_mass = 32
};


/** Data post-processor for the solution of a compressible flow.
 * 
 * More precisely, for a numerical solution vector
 * \f$ \boldsymbol u_h = \big( \rho_h, m^h_1, \dots, m^h_d, E_h \big)^T \f$
 * the following solution data components are extracted
 * 
 * - density \f$ \rho \f$,
 * - velocity \f$ \boldsymbol v = \frac{\boldsymbol m}{\rho} \f$,
 * - pressure \f$ p =
 *   \big( \gamma - 1 \big)\, \big( E - E_{\text{kin}}(\boldsymbol u) \big) \f$,
 * - total energy per unit mass \f$ \boldsymbol e = \frac{E}{\rho} \f$
 * 
 * with post-processing operations for an global output.
 * 
 * Objects from this class can directly attached to the deal.II DataOut write
 * functions and to the DTM++.core DTM::core::DataOutput write function.
 * 
 */
template<int dim, int n_components>
class DataPostprocessor : public dealii::DataPostprocessor<dim> {
public:
	DataPostprocessor(
		const unsigned int output_quantities,
		const unsigned int density_component = 0,
		const unsigned int first_momentum_component = 1,
		const unsigned int energy_component = dim+1,
		const double gamma_minus_one = 0.4) :
		density_component(density_component),
		first_momentum_component(first_momentum_component),
		energy_component(energy_component),
		output_quantities(output_quantities),
		gamma_minus_one(gamma_minus_one),
		uflags(dealii::update_values) {
		////////////////////////////////////////////////////////////////////////
		// set up postprocessed component names and dci
		
		unsigned int internal_component=0;
		
		if (output_quantities & OutputQuantities::density) {
			postprocessed_quantities_names.push_back("density");
			dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
			internal_density_component = internal_component;
			++internal_component;
		}
		else {
			internal_density_component = dealii::numbers::invalid_unsigned_int;
		}
		
		if (output_quantities & OutputQuantities::momentum) {
			internal_first_momentum_component = internal_component;
			
			for (unsigned int k(0); k < dim; ++k) {
				postprocessed_quantities_names.push_back("momentum");
				dci.push_back(
					dealii::DataComponentInterpretation::component_is_part_of_vector
				);
				++internal_component;
			}
		}
		else {
			internal_first_momentum_component = dealii::numbers::invalid_unsigned_int;
		}
		
		if (output_quantities & OutputQuantities::velocity) {
			internal_first_velocity_component = internal_component;
			
			for (unsigned int k(0); k < dim; ++k) {
				postprocessed_quantities_names.push_back("velocity");
				dci.push_back(
					dealii::DataComponentInterpretation::component_is_part_of_vector
				);
				++internal_component;
			}
		}
		else {
			internal_first_velocity_component = dealii::numbers::invalid_unsigned_int;
		}
		
		if (output_quantities & OutputQuantities::pressure) {
			postprocessed_quantities_names.push_back("pressure");
			dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
			internal_pressure_component = internal_component;
			++internal_component;
		}
		else {
			internal_pressure_component = dealii::numbers::invalid_unsigned_int;
		}
		
		if (output_quantities & OutputQuantities::total_energy_volume) {
			postprocessed_quantities_names.push_back("total_energy_per_unit_volume");
			dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
			internal_total_energy_volume_component = internal_component;
			++internal_component;
		}
		else {
			internal_total_energy_volume_component = dealii::numbers::invalid_unsigned_int;
		}
		
		if (output_quantities & OutputQuantities::total_energy_mass) {
			postprocessed_quantities_names.push_back("total_energy_per_unit_mass");
			dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
			internal_total_energy_mass_component = internal_component;
			++internal_component;
		}
		else {
			internal_total_energy_mass_component = dealii::numbers::invalid_unsigned_int;
		}
		
		internal_n_components = internal_component;
		
		Assert(
			internal_n_components > 0,
			dealii::ExcMessage("You specified no output quantity")
		);
	}
	
	virtual ~DataPostprocessor() = default;
	
	
	virtual
	void
	compute_derived_quantities_vector(
		const std::vector< dealii::Vector<double> > &uh,
		const std::vector< std::vector< dealii::Tensor<1,dim> > > &,
		const std::vector< std::vector< dealii::Tensor<2,dim> > > &,
		const std::vector< dealii::Point<dim> > &,
		const std::vector< dealii::Point<dim> > &,
		std::vector< dealii::Vector<double> > &postprocessed_quantities
	) const;
	
	
	virtual std::vector< std::string > get_names() const;
	
	
	virtual
	std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation >
	get_data_component_interpretation() const;
	
	
	virtual dealii::UpdateFlags get_needed_update_flags() const;
	
private:
	/** Compute the kinetic energy \f$ E_{\text{kin}} \f$ from \f$ \boldsymbol u \f$.
	 * @param[in] u Input vector of the solution \f$ \boldsymbol u \f$.
	 * @return Returns the value of the kinetic energy.
	 * 
	 * The kinetic energy \f$ E_{\text{kin}} \f$ of the solution
	 * \f$ \boldsymbol u = \big( \rho, m_1, \dots, m_d, E \big)^T \f$
	 * is derived as
	 * \f[
	 *   E_{\text{kin}} = \frac{1}{2}\, \rho\, | \boldsymbol v |^2\,.
	 * \f]
	 * Since the solution vector \f$ \boldsymbol u \f$ contains the momentum
	 * solution components and not the velocity solution \f$ \boldsymbol v \f$,
	 * we compute here
	 * \f[
	 *   E_{\text{kin}} =
	 *     \frac{1}{2\, \rho}\,
	 *     \sum_{k=1}^{d} m_k^2 \,,\qquad
	 *   m_k = \rho v_k,\quad k=1,\dots,d\,.
	 * \f]
	 * 
	 */
	double
	kinetic_energy(const dealii::Vector<double> &uh) const;
	
	
	/** Compute the pressure \f$ p \f$ from \f$ \boldsymbol u \f$.
	 * 
	 * @param[in] u Input vector of the solution \f$ \boldsymbol u \f$.
	 * @return Returns the value of the pressure \f$ p \f$.
	 * 
	 * The pressure \f$ p \f$ of the solution
	 * \f$ \boldsymbol u = \big( \rho, m_1, \dots, m_d, E \big)^T \f$
	 * is derived as
	 * \f[ p =
	 *   \big( \gamma - 1 \big)\,
	 *   \big( E - E_{\text{kin}}(\boldsymbol u) \big)\,,
	 * \f]
	 * where we denote by \f$ \gamma \f$ the ratio of specific heats of the fluid,
	 * given by EulerEquations< dim >::gamma,<br>
	 * and by \f$ E_{\text{kin}} \f$ the kinetic energy which is computed by
	 * calling DataPostprocessor< dim >::kinetic_energy.
	 * 
	 */
	double
	pressure(const dealii::Vector<double> &uh) const;
	
	const unsigned int density_component = 0;
	const unsigned int first_momentum_component = 1;
	const unsigned int energy_component = dim+1;
	
	unsigned int output_quantities;
	
	unsigned int internal_density_component;
	unsigned int internal_first_momentum_component;
	unsigned int internal_first_velocity_component;
	unsigned int internal_pressure_component;
	unsigned int internal_total_energy_volume_component;
	unsigned int internal_total_energy_mass_component;
	
	unsigned int internal_n_components;
	
	const double gamma_minus_one;
	
	std::vector< std::string > postprocessed_quantities_names;
	std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation > dci;
	const dealii::UpdateFlags uflags;
};


}} // namespace

#endif

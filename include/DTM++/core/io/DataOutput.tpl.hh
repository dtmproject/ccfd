/**
 * @file DataOutput.tpl.hh
 * @author Uwe Koecher (UK)
 * @date 2015-02-25, DTM++.core for DTM++/ccfd, UK
 * @date 2014-06-19, DTM++.core for DTM++ 1.0 MEAT, UK
 * @date 2014-04-29, (2012-08-08), UK
 *
 * @brief This is a template to output a VECTOR as hdf5/xdmf.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __DataOutput_tpl_hh
#define __DataOutput_tpl_hh

// DEAL.II includes
#include <deal.II/base/index_set.h>

#include <deal.II/dofs/dof_handler.h>

#include <deal.II/numerics/data_component_interpretation.h>
#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/data_postprocessor.h>

#include <deal.II/lac/trilinos_block_vector.h>
#include <deal.II/lac/trilinos_vector.h>
#include <deal.II/lac/vector.h>

// C++ includes
#include <memory>
#include <string>
#include <vector>

namespace DTM {
namespace core {

template<int dim, class VectorType>
class DataOutput {
public:
	DataOutput();
	
	constexpr DataOutput(DataOutput &copy)  :
	mpi_comm(copy.mpi_comm),
	format(copy.format),
	dof(copy.dof),
	data_field_names(copy.data_field_names),
	dci_field(copy.dci_field),
	mesh_file_counter(copy.mesh_file_counter),
	data_file_counter(copy.data_file_counter),
	setw_value(copy.setw_value),
	process_id(copy.process_id),
	data_output_patches(copy.data_output_patches),
	xdmf_entries_mesh(copy.xdmf_entries_mesh),
	xdmf_entries_data(copy.xdmf_entries_data),
	mesh_filename(copy.mesh_filename)
	{};
	
	virtual ~DataOutput() = default;
	
	enum class DataFormat {
		invalid,
		HDF5_XDMF
	};
	
	virtual void set_MPI_Comm(MPI_Comm mpi_comm = MPI_COMM_WORLD);
	
	virtual void set_DoF_data(
		std::shared_ptr<dealii::DoFHandler<dim>> dof,
		std::shared_ptr<std::vector<dealii::IndexSet>> partitioning_locally_owned_dofs,
		std::shared_ptr<std::vector<dealii::IndexSet>> partitioning_locally_relevant_dofs
	);
	
	virtual void set_output_format(DataFormat format);
	
	virtual void set_data_output_patches(unsigned int data_output_patches = 1);
	
	/// Set output number width.
	virtual void set_setw(const unsigned int setw_value = 4);
	
	virtual void set_data_field_names(
		std::vector<std::string> &data_field_names
	);
	
	virtual void set_data_component_interpretation_field(
		std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation> &dci_field
	);
	
	/// Writes mesh data and process_id data, must be called before write_data.
	virtual void write_mesh(std::string mesh_name, double time);
	
	/// Writes data vector (without mesh data).
	virtual void write_data(std::string data_name, VectorType &data_vector, double time);
	
	
	virtual void write(
		const std::string &solution_name,
		const dealii::Vector<double> &solution_vector,
		const double &time
	);
	
	virtual void write(
		const std::string &solution_name,
		const dealii::Vector<double> &solution_vector,
		std::shared_ptr< dealii::DataPostprocessor<dim> > data_postprocessor,
		const double &time
	);
	
	virtual void increment_data_file_counter(const unsigned int value = 1) {
		data_file_counter += value;
	}
	
	virtual void increment_mesh_file_counter(const unsigned int value = 1) {
		mesh_file_counter += value;
	}
	
protected:
// 	void write_mesh_hdf5_xdmf(std::string mesh_name, VectorType &,      double time);
// 	void write_data_hdf5_xdmf(std::string data_name, VectorType &value, double time);
	
	void write_mesh_hdf5_xdmf(
		std::string mesh_name,
		dealii::Vector<double> &,
		double time
	);
	
	void write_data_hdf5_xdmf(
		std::string data_name,
		dealii::Vector<double> &value,
		double time
	);
	
	MPI_Comm mpi_comm;
	DataFormat format;
	std::shared_ptr<dealii::DoFHandler<dim>> dof;
	std::shared_ptr<std::vector<dealii::IndexSet>> partitioning_locally_owned_dofs;
	std::shared_ptr<std::vector<dealii::IndexSet>> partitioning_locally_relevant_dofs;
	
	std::vector<std::string> data_field_names;
	std::vector<std::string> data_field_names_process_id;
	std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation> dci_field;
	
	unsigned int mesh_file_counter;
	unsigned int data_file_counter;
	unsigned int setw_value;
	const unsigned int process_id;
	unsigned int data_output_patches;
	
	std::vector<dealii::XDMFEntry> xdmf_entries_mesh;
	std::vector<dealii::XDMFEntry> xdmf_entries_data;
	
	std::string mesh_filename;
};

}} // namespaces

#endif

/**
 * @file DataOutput.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-25, DTM++.core for DTM++/ccfd, UK
 * @date 2014-06-19, DTM++.core for DTM++ 1.0 MEAT, UK
 * @date 2014-04-29, (2012-08-08), UK
 *
 * @brief This is a template to output a VECTOR as hdf5/xdmf.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

// DTM++ includes
#include <DTM++/core/io/DataOutput.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/mpi.h>
#include <deal.II/base/index_set.h>

#include <deal.II/dofs/dof_tools.h>
#include <deal.II/dofs/dof_renumbering.h>

#include <deal.II/lac/trilinos_block_vector.h>
#include <deal.II/lac/trilinos_vector.h>


// C++ includes
#include <memory>
#include <string>
#include <vector>
#include <fstream>


namespace DTM {
namespace core {

template<int dim, class VectorType>
DataOutput<dim,VectorType>::
DataOutput() :
	process_id(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD)) {
	set_MPI_Comm();
	set_setw();
	
#ifdef DEAL_II_WITH_HDF5
	set_output_format(DataFormat::HDF5_XDMF);
#else
	set_output_format(DataFormat::invalid);
#endif
	
	set_data_output_patches();
	
	mesh_file_counter = 0;
	data_file_counter = 0;
	
	xdmf_entries_mesh.clear();
	xdmf_entries_data.clear();
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
set_MPI_Comm(MPI_Comm _mpi_comm) {
	mpi_comm = _mpi_comm;
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
set_DoF_data(
	std::shared_ptr<dealii::DoFHandler<dim>> _dof,
	std::shared_ptr<std::vector<dealii::IndexSet>> _partitioning_locally_owned_dofs,
	std::shared_ptr<std::vector<dealii::IndexSet>> _partitioning_locally_relevant_dofs ) {
	dof = _dof;
	partitioning_locally_owned_dofs = _partitioning_locally_owned_dofs;
	partitioning_locally_relevant_dofs = _partitioning_locally_relevant_dofs;
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
set_output_format(DataFormat _format) {
	format = _format;
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
set_data_output_patches(unsigned int _data_output_patches) {
	data_output_patches = _data_output_patches;
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
set_setw(const unsigned int _setw_value) {
	setw_value = _setw_value;
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
set_data_field_names(
	std::vector<std::string> &_data_field_names) {
	data_field_names.resize(_data_field_names.size());
	
	auto it_data_field_names = _data_field_names.begin();
	for (auto &data_field_name : data_field_names) {
		data_field_name = *it_data_field_names;
		++it_data_field_names;
	}
	
	data_field_names_process_id.resize(data_field_names.size());
	it_data_field_names = _data_field_names.begin();
	for (auto &data_field_name : data_field_names_process_id) {
		data_field_name = "process_id__" + *it_data_field_names;
		++it_data_field_names;
	}
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
set_data_component_interpretation_field(
	std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation> &_dci_field) {
	dci_field.resize(_dci_field.size());
	
	auto it_dci_field = _dci_field.begin();
	for (auto &dci : dci_field) {
		dci = *it_dci_field;
		++it_dci_field;
	}
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
write_mesh(std::string mesh_name, double time) {
	VectorType vec;
	
	if (format == DataFormat::HDF5_XDMF) {
		write_mesh_hdf5_xdmf(mesh_name, vec, time);
	}
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
write_data(std::string data_name, VectorType &data_vector, double time) {
	if (format == DataFormat::HDF5_XDMF) {
		write_data_hdf5_xdmf(data_name, data_vector, time);
	}
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
write(
	const std::string &solution_name,
	const dealii::Vector<double> &solution_vector,
	const double &time) {
	Assert(format == DataFormat::HDF5_XDMF, dealii::ExcNotImplemented());
	
	// prepare output filenames
	std::ostringstream stream;
	stream.str("");
	stream << solution_name << "_mesh" << "_"
		<< std::setw(setw_value) << std::setfill('0') << mesh_file_counter
		<< ".h5";
	// save mesh_filename into member variable for further use by write_data
	mesh_filename = stream.str();
	
	stream.str("");
	stream << solution_name << "_"
		<< std::setw(setw_value) << std::setfill('0') << data_file_counter
		<< ".h5";
	std::string data_filename = stream.str();
	
	stream.str("");
	stream << solution_name
		<< ".xdmf";
	std::string xdmf_filename = stream.str();
	
	// create dealii::DataOut object and attach the data
	dealii::DataOut<dim> data_out;
	data_out.attach_dof_handler(*dof);
	
	data_out.add_data_vector(
		solution_vector,
		data_field_names,
		dealii::DataOut<dim>::type_dof_data,
		dci_field
	);
	data_out.build_patches(data_output_patches);
	
	// Filter duplicated verticies = true, hdf5 = true
	dealii::DataOutBase::DataOutFilter data_filter(
		dealii::DataOutBase::DataOutFilterFlags(false, true)
	);
	
	// Filter the data and store it in data_filter
	data_out.write_filtered_data(data_filter);
	
	// Write the filtered data to HDF5
	data_out.write_hdf5_parallel(
		data_filter,
		true, // write mesh data
		mesh_filename.c_str(),
		data_filename.c_str(),
		mpi_comm
	);
	
	// Add XDMF entry
	xdmf_entries_data.push_back(
		data_out.create_xdmf_entry(
			data_filter,
			mesh_filename.c_str(),
			data_filename.c_str(),
			time,
			mpi_comm
		)
	);
	
	data_out.write_xdmf_file(
		xdmf_entries_data,
		xdmf_filename.c_str(),
		mpi_comm
	);
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
write(
	const std::string &solution_name,
	const dealii::Vector<double> &solution_vector,
	std::shared_ptr< dealii::DataPostprocessor<dim> > data_postprocessor,
	const double &time) {
	Assert(format == DataFormat::HDF5_XDMF, dealii::ExcNotImplemented());
	
	// prepare output filenames
	std::ostringstream stream;
	stream.str("");
	stream << solution_name << "_mesh" << "_"
		<< std::setw(setw_value) << std::setfill('0') << mesh_file_counter
		<< ".h5";
	// save mesh_filename into member variable for further use by write_data
	mesh_filename = stream.str();
	
	stream.str("");
	stream << solution_name << "_"
		<< std::setw(setw_value) << std::setfill('0') << data_file_counter
		<< ".h5";
	std::string data_filename = stream.str();
	
	stream.str("");
	stream << solution_name
		<< ".xdmf";
	std::string xdmf_filename = stream.str();
	
	// create dealii::DataOut object and attach the data
	dealii::DataOut<dim> data_out;
	data_out.attach_dof_handler(*dof);
	
	data_out.add_data_vector(
		solution_vector,
		*data_postprocessor
	);
	data_out.build_patches(data_output_patches);
	
	// Filter duplicated verticies = true, hdf5 = true
	dealii::DataOutBase::DataOutFilter data_filter(
		dealii::DataOutBase::DataOutFilterFlags(false, true)
	);
	
	// Filter the data and store it in data_filter
	data_out.write_filtered_data(data_filter);
	
	// Write the filtered data to HDF5
	data_out.write_hdf5_parallel(
		data_filter,
		true, // write mesh data
		mesh_filename.c_str(),
		data_filename.c_str(),
		mpi_comm
	);
	
	// Add XDMF entry
	xdmf_entries_data.push_back(
		data_out.create_xdmf_entry(
			data_filter,
			mesh_filename.c_str(),
			data_filename.c_str(),
			time,
			mpi_comm
		)
	);
	
	data_out.write_xdmf_file(
		xdmf_entries_data,
		xdmf_filename.c_str(),
		mpi_comm
	);
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
write_mesh_hdf5_xdmf(
	std::string mesh_name,
	dealii::Vector<double> &vec,
	double time) {
	// create vector containing subdomain id's
	dealii::Vector<double> process_id_vec;
	process_id_vec.reinit(vec);
	process_id_vec = process_id;
	
	// prepare output filenames
	std::ostringstream stream;
	stream.str("");
	stream << mesh_name << "_"
		<< std::setw(setw_value) << std::setfill('0') << mesh_file_counter
		<< ".h5";
	// save mesh_filename into member variable for further use by write_data
	mesh_filename = stream.str();
	
	stream.str("");
	stream << "process_id" << "_"
		<< std::setw(setw_value) << std::setfill('0') << mesh_file_counter
		<< ".h5";
	std::string process_id_filename = stream.str();
	
	stream.str("");
	stream << "process_id"
		<< ".xdmf";
	std::string xdmf_filename = stream.str();
	
	// create dealii::DataOut object and attach the data
	dealii::DataOut<dim> data_out;
	data_out.attach_dof_handler(*dof);
	
	data_out.add_data_vector(
		process_id_vec,
		data_field_names_process_id,
		dealii::DataOut<dim>::type_dof_data,
		dci_field
	);
	data_out.build_patches(data_output_patches);
	
	// Filter duplicated verticies = true, hdf5 = true
	dealii::DataOutBase::DataOutFilter data_filter(
		dealii::DataOutBase::DataOutFilterFlags(false, true)
	);
	
	// Filter the data and store it in data_filter
	data_out.write_filtered_data(data_filter);
	
	// Write the filtered data to HDF5
	data_out.write_hdf5_parallel(
		data_filter,
		true, // write mesh data
		mesh_filename.c_str(),
		process_id_filename.c_str(),
		mpi_comm
	);
	
	// Add XDMF entry
	xdmf_entries_mesh.push_back(
		data_out.create_xdmf_entry(
			data_filter,
			mesh_filename.c_str(),
			process_id_filename.c_str(),
			time,
			mpi_comm
		)
	);
	
	data_out.write_xdmf_file(
		xdmf_entries_mesh,
		xdmf_filename.c_str(),
		mpi_comm
	);
	
// 	mesh_file_counter++;
}


template<int dim, class VectorType>
void
DataOutput<dim,VectorType>::
write_data_hdf5_xdmf(
	std::string data_name,
	dealii::Vector<double> &x,
	double time) {
	// prepare output filenames
	std::ostringstream stream;
	stream.str("");
	stream << data_name << "_"
		<< std::setw(setw_value) << std::setfill('0') << data_file_counter
		<< ".h5";
	std::string data_filename = stream.str();
	
	stream.str("");
	stream << data_name
		<< ".xdmf";
	std::string xdmf_filename = stream.str();
	
	// create dealii::DataOut object and attach the data
	dealii::DataOut<dim> data_out;
	data_out.attach_dof_handler(*dof);
	
	data_out.add_data_vector(
		x,
		data_field_names,
		dealii::DataOut<dim>::type_dof_data,
		dci_field
	);
	data_out.build_patches(data_output_patches);
	
	// Filter duplicated verticies = true, hdf5 = true
	dealii::DataOutBase::DataOutFilter data_filter(
		dealii::DataOutBase::DataOutFilterFlags(false, true)
	);
	
	// Filter the data and store it in data_filter
	data_out.write_filtered_data(data_filter);
	
	// Write the filtered data to HDF5
	data_out.write_hdf5_parallel(
		data_filter,
		false, // write mesh data
		mesh_filename.c_str(),
		data_filename.c_str(),
		mpi_comm
	);
	
	// Add XDMF entry
	xdmf_entries_data.push_back(
		data_out.create_xdmf_entry(
			data_filter,
			mesh_filename.c_str(),
			data_filename.c_str(),
			time,
			mpi_comm
		)
	);
	
	data_out.write_xdmf_file(
		xdmf_entries_data,
		xdmf_filename.c_str(),
		mpi_comm
	);
	
// 	data_file_counter++;
}

}} // namespaces

#include "DataOutput.inst.in"

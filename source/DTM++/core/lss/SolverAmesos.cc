/**
 * @file SolverAmesos.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-02, Rebase to DTM++.core library, UK
 * @date 2015-01-23, meat application, UK
 * @date 2014-09-12, UK
 * @date 2014-06-14, UK
 *
 * @brief SolverAmesos.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

// PROJECT includes
#include <DTM++/core/lss/SolverAmesos.hh>

// MPI includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>
#include <deal.II/base/logstream.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/solver.h>

// TRILINOS includes

// C++ includes
#include <memory>
#include <iostream>

namespace DTM {
namespace core {
namespace lss {


SolverAmesos::
SolverAmesos(
	std::shared_ptr<dealii::SolverControl> sc,
	std::shared_ptr<Teuchos::ParameterList> solver_parameters) :
	solver_control(sc) {
	// push "SolverAmesos" prefix to the deallog stream
	dealii::deallog.push("SolverAmesos");
	
	// check if dealii::SolverControl was given, if not, create a default one
	if (!solver_control.use_count()) {
		solver_control = std::make_shared<dealii::SolverControl> (100, 1e-10, false, false);
		dealii::deallog
			<< "created dealii::SolverControl object"
			<< ", max_steps = " << solver_control->max_steps()
			<< ", tolerance = " << solver_control->tolerance()
			<< std::endl;
	}
	
	if (!solver_parameters.use_count()) {
		SolverType = "Amesos_Superludist";
	}
	
	// output the Amesos solver settings to deallog
	dealii::deallog << "Amesos solver type: ";
	dealii::deallog << SolverType;
	dealii::deallog << std::endl;
	
	dealii::deallog.pop();
}


void
SolverAmesos::
factorise(
	std::shared_ptr<const dealii::TrilinosWrappers::SparseMatrix> A,
	std::shared_ptr<dealii::TrilinosWrappers::MPI::Vector> x,
	std::shared_ptr<const dealii::TrilinosWrappers::MPI::Vector> b) {
	int ierr;
	
	// Create and set linear problem.
	linear_problem = std::make_shared<Epetra_LinearProblem> (
			const_cast<Epetra_CrsMatrix *>( &(A->trilinos_matrix()) ),
			&(x->trilinos_vector()),
			const_cast<Epetra_MultiVector *>( &(b->trilinos_vector()) )
	);
	
	// create the solver instance
	Amesos Factory;
	AssertThrow(
		Factory.Query(SolverType.c_str()),
		dealii::ExcMessage (std::string ("You tried to select the solver type <") +
					SolverType +
					"> but this solver is not supported by Trilinos either "
					"because it does not exist, or because Trilinos was not "
					"configured for its use.")
	);
	
	solver.reset(
		Factory.Create(SolverType.c_str(), *linear_problem)
	);
	
// 	// set solver parameters from Teuchos::ParameterList, if present,
// 	// otherwise the Amesos solver uses the default values (GMRES, AZ_kspace=30)
// 	if (solver_parameters.use_count()) {
// 		solver->SetParameters(*solver_parameters);
// 	}
	
	std::shared_ptr<Teuchos::ParameterList> solver_parameters =
		std::make_shared<Teuchos::ParameterList>();
	Teuchos::ParameterList& SuperludistParams =
		solver_parameters->sublist("Superludist");
	SuperludistParams.set<bool>("PrintNonzeros", false);
	
	solver->SetParameters(*solver_parameters);
	
	//
	ierr = solver->SymbolicFactorization();
    AssertThrow (ierr == 0, dealii::ExcTrilinosError(ierr));
	
	ierr = solver->NumericFactorization();
	AssertThrow (ierr == 0, dealii::ExcTrilinosError(ierr));
}


void
SolverAmesos::
solve() {
	int ierr;
	
	ierr = solver->Solve();
	AssertThrow (ierr == 0, dealii::ExcTrilinosError(ierr));

	// Finally, let the deal.II SolverControl object know what has
	// happened. If the solve succeeded, the status of the solver control will
	// turn into SolverControl::success.
	solver_control->check (0, 0);
}


}}} // namespaces

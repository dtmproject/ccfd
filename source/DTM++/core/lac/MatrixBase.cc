/**
 * @file MatrixBase.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-02, Rebase to DTM++.core library, UK
 * @date 2015-01-23, meat application, UK
 * @date 2014-06-14, UK
 *
 * @brief MatrixBase.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

// PROJECT includes
#include <DTM++/core/lac/MatrixBase.hh>
#include "MatrixBase.inst.in"

// MPI includes

// DEAL.II includes
#include <deal.II/base/exceptions.h>

// TRILINOS includes

// C++ includes

namespace DTM {
namespace core {
namespace lac {

template<class VectorType>
MatrixBase<VectorType>::
MatrixBase() {
}


template<class VectorType>
MatrixBase<VectorType>::
~MatrixBase() {
}


template<class VectorType>
void
MatrixBase<VectorType>::
vmult(VectorType &dst, const VectorType &src) const {
	AssertThrow(false, dealii::ExcNotImplemented());
}


template<class VectorType>
void
MatrixBase<VectorType>::
Tvmult(VectorType &dst, const VectorType &src) const {
	AssertThrow(false, dealii::ExcNotImplemented());
}


template<class VectorType>
void
MatrixBase<VectorType>::
vmult_add(VectorType &dst, const VectorType &src) const {
	AssertThrow(false, dealii::ExcNotImplemented());
}


template<class VectorType>
void
MatrixBase<VectorType>::
Tvmult_add(VectorType &dst, const VectorType &src) const {
	AssertThrow(false, dealii::ExcNotImplemented());
}

}}}

/**
 * @file   ConservationLaw.tpl.cc
 * @author Uwe Koecher (UK)
 * @date   2015-02-10, UK
 *
 * @brief DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/CCFD (Comput. Compressible Fluid Dynamics).    */
/*                                                                            */
/*  DTM++/CCFD is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/CCFD is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/CCFD.   If not, see <http://www.gnu.org/licenses/>.      */

/* ORIGIN of this code was (partly) deal.II/example/step-33/step-33.cc:
 * ---------------------------------------------------------------------
 *
 * Copyright (C) 2007 - 2015 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 *
 * Author: David Neckels, Boulder, Colorado, 2007, 2008
 */


// PROJECT INCLUDES
#include <ccfd/ConservationLaw.tpl.hh>

// DEAL.II includes
#include <deal.II/base/function.h>
#include <deal.II/base/mpi.h>
#include <deal.II/base/parameter_handler.h>
#include <deal.II/base/table.h>
#include <deal.II/base/utilities.h>

#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_q.h>

#include <deal.II/grid/grid_in.h>

#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/trilinos_solver.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/solution_transfer.h>
#include <deal.II/numerics/vector_tools.h>

// C++ includes
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

namespace ccfd {
namespace euler {

/** Constructor. Initialize member variables.
 *  @var mapping         Init: Default.
 *  @var fe              Init: Is of type FE_System. Continuous piecewiese Linear FE_Q(1) for all (dim+2) components.
 *  @var dof_handler     Init: Default.
 *  @var quadrature      Init: 2 quadrature points
 *  @var face_quadrature Init: 2 quadrature points
 *  @var verbose_cout    Init: output on std::cout for only process with rank 0.
 */
template<int dim, int n_components>
ConservationLaw<dim,n_components>::
ConservationLaw() :
	// begin member init
	p(1), // polynomial degree (only p=1 is implemented!)
	mapping(p, (p > 1 ? true : false)), // exterior and interior boundary mapping
	fe(dealii::FE_Q<dim>(p), n_components),
	quadrature(p+1),
	face_quadrature(p+1),
	
	
	verbose_cout(
		std::cout,
		!(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD))
	),
	pcout (
		std::cout,
		!(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD))
	),
	MyPID(dealii::Utilities::MPI::this_mpi_process(MPI_COMM_WORLD))
	// end member init
{
	dof = std::make_shared< dealii::DoFHandler<dim> > (triangulation);

	// TODO:
// 	verbose_cout.set_condition(
// 		parameters.output == Parameters::Solver::verbose
// 	);
	verbose_cout.set_condition(
		false
	);
	
	input_parameters_set = false;
}


template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
set_input_parameters(dealii::ParameterHandler &parameters) {
	parameters.enter_subsection("Time Integration"); {
		tau = parameters.get_double("initial time step size");
		
		tau_min = parameters.get_double("minimal time step size");
		
		tau_max = parameters.get_double("maximal time step size");
		
		theta = parameters.get_double("theta");
		
		t0 = parameters.get_double("initial time");
		
		T = parameters.get_double("final time");
	}
	parameters.leave_subsection();
	
	// Grid parameters
	parameters.enter_subsection("Grid"); {
		mesh_filename = parameters.get("grid in filename");
		
		h_adaptivity_enabled = parameters.get_bool("enable h-adaptivity");
	}
	parameters.leave_subsection();
	
	// Shock Capturing parameters
	parameters.enter_subsection("Shock Capturing"); {
		shock_caputuring_enabled = parameters.get_bool("enable shock capturing");
		shock_levels = parameters.get_double("shock levels");
		shock_value = parameters.get_double("shock value");
	}
	parameters.leave_subsection();
	
	// Streamline Diffusion parameters
	parameters.enter_subsection("Streamline Diffusion"); {
		streamline_diffusion_power = parameters.get_double("streamline diffusion power");
	}
	parameters.leave_subsection();
	
	input_parameters_set = true;
}


template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
numerical_normal_flux(
	const SolutionType<n_components> &u_plus,
	const SolutionType<n_components> &u_minus,
	const dealii::Tensor<1,dim> normal,
	const double alpha,
	FluxFunctionVectorType<n_components> &normal_flux
) {
	FluxMatrixType<n_components,dim> iflux;
	FluxMatrixType<n_components,dim> oflux;
	
	flux_matrix(u_plus, iflux);
	flux_matrix(u_minus, oflux);
	
	for (unsigned int i(0); i < n_components; ++i) {
		normal_flux[i] = 0;
		
		for (unsigned int j(0); j < dim; ++j) {
			normal_flux[i] +=
				0.5 * ( iflux[i][j] + oflux[i][j] ) * normal[j];
		}
		
		normal_flux[i] += 0.5 * alpha * (u_plus[i] - u_minus[i]);
	}
	
}


/** Set up sparsity pattern and reinit system matrix after computational mesh change.
 * The following function must be called each time the mesh is changed.
 */
template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
setup_system() {
	dealii::DynamicSparsityPattern sparsity_pattern(
		dof->n_dofs(),
		dof->n_dofs()
	);
	
	dealii::DoFTools::make_sparsity_pattern(
		*dof,
		sparsity_pattern
	);
	
	system_matrix.reinit(sparsity_pattern);
}


/** Assemble system matrix and right-hand-side vector.
 * This function puts all of the assembly pieces together in a routine
 * that dispatches the correct piece for each cell and face, respectively.
 * 
 * Therefore, it calls
 *   - ConservationLaw< dim, n_components >::assemble_cell_term  for the cell assemblies, and
 *   - ConservationLaw< dim, n_components >::assemble_face_term  for the face assemblies.
 */
template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
assemble_system() {
	const unsigned int dofs_per_cell = dof->get_fe().dofs_per_cell;
	
	std::vector<dealii::types::global_dof_index> dofs(dofs_per_cell);
	std::vector<dealii::types::global_dof_index> dofs_neighbor(dofs_per_cell);
	
	const dealii::UpdateFlags update_flags
	=	dealii::update_values
	|	dealii::update_gradients
	|	dealii::update_q_points
	|	dealii::update_JxW_values;
	
	const dealii::UpdateFlags face_update_flags
	=	dealii::update_values
	|	dealii::update_q_points
	|	dealii::update_normal_vectors
	|	dealii::update_JxW_values;
	
	const dealii::UpdateFlags neighbor_face_update_flags
	=	dealii::update_values;
	
	// Set up FE-Values objects for cell assemblies and face assemblies.
	dealii::FEValues<dim> fe_values(
		mapping,
		fe,
		quadrature,
		update_flags
	);
	
	dealii::FEFaceValues<dim> fe_face_values(
		mapping,
		fe,
		face_quadrature,
		face_update_flags
	);
	
	dealii::FESubfaceValues<dim> fe_subface_values(
		mapping,
		fe,
		face_quadrature,
		face_update_flags
	);
	
	dealii::FEFaceValues<dim> fe_face_values_neighbor(
		mapping,
		fe,
		face_quadrature,
		neighbor_face_update_flags
	);
	
	dealii::FESubfaceValues<dim> fe_subface_values_neighbor(
		mapping,
		fe,
		face_quadrature,
		neighbor_face_update_flags
	);
	
	// Then loop over all cells, initialize the FEValues object for the
	// current cell and call the function that assembles the problem on this
	// cell.
	auto cell = dof->begin_active();
	auto endc = dof->end();
	
	for (; cell!=endc; ++cell) {
		fe_values.reinit(cell);
		
		// store a vector of the global dof indices located at this cell.
		cell->get_dof_indices(dofs);
		
		assemble_cell_term(fe_values, dofs);
		
		// Then loop over all faces of this cell.
		for (unsigned int face_no(0);
			 face_no < dealii::GeometryInfo<dim>::faces_per_cell;
			 ++face_no) {
			
			if (cell->at_boundary(face_no)) {
				// Boundary (external) face.
				// 
				// If a face is part of the external boundary,
				// then assemble boundary conditions there
				//   (the fifth argument to <code>assemble_face_terms</code> indicates
				//    whether we are working on an external or internal face;
				//    if it is an external face, the fourth argument denoting the
				//    degrees of freedom indices of the neighbor is ignored,
				//    so we pass an empty vector):
				
				// initialize FE-Values of a boundary face
				fe_face_values.reinit(cell, face_no);
				
				// assemble terms of a boundary face (without neighbor face)
				assemble_face_term(
					face_no,
					fe_face_values,
					fe_face_values,
					dofs,
					std::vector<dealii::types::global_dof_index>(), // neighbor
					false,
					cell->face(face_no)->boundary_id(),
					cell->face(face_no)->diameter()
				);
			}
			else {
				// Internal face.
				// 
				// There are TWO CASES that we need to distinguish:
				//   1st) that this is a normal face between two cells at the
				//        same refinement level, and
				// 
				//   2nd) that it is a face between two cells of the different
				//        refinement levels.
				// 
				// Precisely:
				//
				// In the first case, there is nothing we need to do: we are using a
				// continuous finite element, and face terms do not appear in the
				// bilinear form in this case.
				// 
				// The second case usually does not lead to face terms either
				// if we enforce hanging node constraints strongly.
				// 
				// *************************************************************
				// In the current program, however, we opt to ENFORCE CONTINUITY
				// WEAKLY at faces between cells of different refinement level,
				// for two reasons:
				//   (i)  because we are allowed here, and more importantly,
				//   (ii) because we would have to thread the automatic differentiation
				//        we use to compute the elements of the Newton matrix
				//        from the residual through the operations of the
				//        ConstraintMatrix class.
				// *************************************************************
				// 
				// What needs to be decided is which side of an interface between
				// two cells of different refinement level we are sitting on.
				// 
				if (cell->neighbor(face_no)->has_children()) {
					// (a) case: The neighbor is more refined.
					//           We then have to loop over the children of the face
					//           of the current cell and integrate on each of them.
					//           We sprinkle a couple of assertions into the code to
					//           ensure that our reasoning trying to figure out
					//           which of the neighbor's children's faces coincides
					//           with a given subface of the current cell's faces is
					//           correct.
					
					// Backtrace the neighbor of the neighbor as:
					const unsigned int face_no_neighbor_of_neighbor
					= cell->neighbor_of_neighbor(face_no);
					// NOTE: cell->neighbor_of_neighbor(face_no) returns the
					//       how-many'th neighbor this cell is of
					//       cell->neighbor(neighbor),
					//       i.e. return the face_no such that
					//       cell->neighbor(neighbor)->neighbor(face_no)==cell.
					//       
					//       This function is the right one if you want to know
					//       how to get back from a neighbor to the present cell.
					
					// Now loop over all faces of the neighbor cell, which are
					// adjacent to the face of the original cell
					for (unsigned int subface_no(0);
						subface_no < cell->face(face_no)->n_children();
						++subface_no) {
						
						// Initialize (active) child cell of neighbor
// 						const typename dealii::DoFHandler<dim>::active_cell_iterator
						auto neighbor_child =
							cell->neighbor_child_on_subface(face_no, subface_no);
						
						// Check for consistency.
						Assert(
							neighbor_child->face(face_no_neighbor_of_neighbor)
							== cell->face(face_no)->child(subface_no),
							dealii::ExcInternalError()
						);
						
						// Check for consistency.
						Assert(
							neighbor_child->has_children() == false,
							dealii::ExcInternalError()
						);
						
						fe_subface_values.reinit(
							cell,
							face_no,
							subface_no
						);
						
						fe_face_values_neighbor.reinit(
							neighbor_child,
							face_no_neighbor_of_neighbor
						);
						
						neighbor_child->get_dof_indices(dofs_neighbor);
						
						assemble_face_term(
							face_no,
							fe_subface_values,
							fe_face_values_neighbor,
							dofs,
							dofs_neighbor,
							true,
							dealii::numbers::invalid_unsigned_int,
							neighbor_child->face(face_no_neighbor_of_neighbor)->diameter()
						);
					} // for subface of neighbor
				} // if (a) case.
				else if (cell->neighbor(face_no)->level() != cell->level()) {
					// (b) case: The neighbor is coarser.
					//           (in particular, because of the usual restriction
					//            of only one hanging node per face, the
					//            neighbor must be exactly one level coarser than
					//            the current cell;
					//            something that we check with an assertion).
					
					// Get the iterator of the neighbor cell.
// 					const typename dealii::DoFHandler<dim>::cell_iterator
					auto neighbor = cell->neighbor(face_no);
					
					// Check if the 2:1 mesh balance of deal.II is fulfilled.
					Assert(
						neighbor->level() == cell->level()-1,
						dealii::ExcInternalError()
					);
					
					neighbor->get_dof_indices(dofs_neighbor);
					
					// Backtrace: Get the face_no and subface_no of the original cell.
					const std::pair<unsigned int, unsigned int> faceno_subfaceno
					= cell->neighbor_of_coarser_neighbor(face_no);
					// 
					// Store them in more useful const variables:
					const unsigned int neighbor_face_no    = faceno_subfaceno.first;
					const unsigned int neighbor_subface_no = faceno_subfaceno.second;
					
					// Check for consistency.
					// (Do the backtrace and compare if iterators match.)
					Assert(
						neighbor->neighbor_child_on_subface(
							neighbor_face_no,
							neighbor_subface_no
						) == cell,
						dealii::ExcInternalError()
					);
					
					fe_face_values.reinit(
						cell,
						face_no
					);
					
					fe_subface_values_neighbor.reinit(
						neighbor,
						neighbor_face_no,
						neighbor_subface_no
					);
					
					assemble_face_term(
						face_no,
						fe_face_values,
						fe_subface_values_neighbor,
						dofs,
						dofs_neighbor,
						true,
						dealii::numbers::invalid_unsigned_int,
						cell->face(face_no)->diameter()
					);
				} // if (b) case
			} // internal faces
		} // for loop over all faces
	} // for loop over all cells
	
	// After all this assembling, notify the Trilinos matrix object that the
	// matrix is done:
	system_matrix.compress(dealii::VectorOperation::add);
}


/** ConservationLaw::assemble_cell_term
 * 
 * This function assembles the cell term by computing the cell part of the
 * residual, adding its negative to the right hand side vector, and adding
 * its derivative with respect to the local variables to the Jacobian
 * (i.e. the Newton matrix).
 * 
 * Recall that the cell contributions to the residual read
 *  \f[ F_i =   \left(\frac{\mathbf{w}_{n+1} - \mathbf{w}_n}{\delta t},
 *                   \mathbf{z}_i \right)_K
 *            - \left(\mathbf{F}(\tilde{\mathbf{w}}),
 *                    \nabla\mathbf{z}_i\right)_K
 *          + h^{\eta} \left( \nabla \mathbf{w}, \nabla \mathbf{z}_i \right)_K
 *          - \left(\mathbf{G}(\tilde{\mathbf w}), \mathbf{z}_i \right)_K \f]
 * where
 *   \f$ \tilde{\mathbf{w}} \f$
 * is represented by the variable <code>u_theta</code>,
 *   \f$ \mathbf{z}_i \f$
 * is the \f$ i \f$-th test function, and the scalar product
 *   \f[ \left(\mathbf{F}(\tilde{\mathbf{w}}), \nabla\mathbf{z}\right)_K \f]
 * is understood as
 *   \f[ \int_K \, \sum_{c=1}^{\text{n_components}} \,
 *    \sum_{d=1}^{\text{dim}} \mathbf{F}(\tilde{\mathbf{w}})_{cd}
 *    \frac{\partial z_c}{\partial x_d} \f]
 */
template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
assemble_cell_term(
	const dealii::FEValues<dim> &fe_values,
	const std::vector<dealii::types::global_dof_index> &dofs
) {
	const unsigned int dofs_per_cell = fe_values.dofs_per_cell;
	const unsigned int n_q_points    = fe_values.n_quadrature_points;
	
	// Format in which we store variables.
	// 
	// (i) : Values at each quadrature point for each of the
	//        <code>n_components</code> components of the
	//        solution vector.
	// 
	// (ii): Automatic differentiation.
	//       We use the Sacado::Fad::DFad template, i.e.
	//       - Sacado: an AD package of Trilinos
	//       - Fad   : forward mode AD classes
	//       - DFad  : (dynamic memory allocation) the number of derivative
	//                 components can be chosen at run-time (most flexible, but slow).
	//       for everything that is computed from the variables with respect to
	//       which we would like to compute derivatives.
	//       This includes the current solution and gradient at the
	//       quadrature points (which are linear combinations of the degrees of
	//       freedom) as well as everything that is computed from them such as
	//       the residual, but not the previous time step's solution.
	
	// Allocate variables that will hold the values of:
	// 
	// the current solution $u_{n+1}^k$
	// after the $k$th Newton iteration (variable <code>u</code>),
	std::vector< SolutionType<n_components> > u(n_q_points);
	// 
	// the previous time step's solution $u_{n}$ (variable <code>u_old</code>),
	std::vector< std::array<double, n_components> > u_old(n_q_points);
	// 
	// as well as the linear combination
	//   $\theta u_{n+1}^k + (1-\theta)u_n$
	// that results from choosing different time stepping schemes
	// (variable <code>u_theta</code>).
	std::vector< SolutionType<n_components> > u_theta(n_q_points);
	
	
	// Gradients of the current variables.
	// We DO NEED these for the diffusion stabilization.
	std::vector< SolutionJacobianType<n_components,dim> > grad_u(n_q_points);
	
	
	std::vector<double> residual_derivatives(dofs_per_cell);
	
	// Define the independent variables that we will try to determine by
	// solving a Newton step.
	// 
	// Therefore, we create a std::vector structure with
	//   <code>dofs_per_cell</code>
	// entries denoted to be able to automatically diff'ed in Forward-AD mode.
	std::vector< Sacado::Fad::DFad<double> > independent_local_dof_values(
		dofs_per_cell
	);
	// 
	// Numerical initialization for the AD-variables:
	for (unsigned int i(0); i < dofs_per_cell; ++i) {
		independent_local_dof_values[i] = current_solution(dofs[i]);
	}
	// 
	// Mark a subset of the AD-variables as independent (local) degrees of freedom:
	for (unsigned int i(0); i < dofs_per_cell; ++i) {
		independent_local_dof_values[i].diff(i, dofs_per_cell);
	}
	
	// *************************************************************************
	// ASSEMBLE
	
	// Prepare data structures for the assembly.
	
	// First, compute the values of
	//   <code>u</code>,
	//   <code>u_old</code>,
	//   <code>u_theta</code>, and
	//   <code>grad_u</code>,
	// which we can compute from the local DoF values by using the formula
	//   $u(x_q)=\sum_i \mathbf u_i \Phi_i(x_q)$,
	// where $\mathbf u_i$ is the $i$th entry of the (local part of the) solution
	// vector, and
	//   $\Phi_i(x_q)$
	// is the value of the $i$th vector-valued shape function evaluated at
	// quadrature point $x_q$.
	// 
	// The gradient can be computed in a similar way.
	// 
	// Initilize variable entries with value "0":
	for (unsigned int q(0); q < n_q_points; ++q) {
		for (unsigned int c(0); c < n_components; ++c) {
			u_old[q][c] = 0;
			u_theta[q][c] = 0;
			u[q][c] = 0;
			
			for (unsigned int k(0); k<dim; ++k) {
				grad_u[q][c][k] = 0;
			}
		}
	}
	// 
	// Compute variable entries:
	for (unsigned int q(0); q < n_q_points; ++q) {
		for (unsigned int i(0); i < dofs_per_cell; ++i) {
			// Vector-valued problem.
			// Get the component index of a local degree of freedom
			const unsigned int c =
				fe_values.get_fe().system_to_component_index(i).first;
			
			u_old[q][c] +=
				old_solution(dofs[i]) *
				fe_values.shape_value_component(i, q, c);
			
			u[q][c] +=
				independent_local_dof_values[i] *
				fe_values.shape_value_component(i, q, c);
			
			
			const dealii::Tensor<1,dim> grad_phi =
				fe_values.shape_grad_component(i, q, c);
			
			for (unsigned int k(0); k < dim; k++) {
				grad_u[q][c][k] +=
					independent_local_dof_values[i] * grad_phi[k];
			}
			
			u_theta[q][c] +=
				( theta * independent_local_dof_values[i]
				+ (1-theta) * old_solution(dofs[i]) )
				* fe_values.shape_value_component(i, q, c);
		}
	}
	
	// Next, in order to compute the cell contributions, we need to evaluate
	//   $F(\tilde{\mathbf w})$
	// and
	//   $G(\tilde{\mathbf w})$
	// at all quadrature points.
	// 
	// To store these, we also need to allocate the memory.
	// NOTE: We compute the flux matrices and right hand sides in terms of
	//       autodifferentiation variables, so that the Jacobian contributions
	//       can  later easily be computed from it.
	//
	std::vector< FluxMatrixType<n_components,dim> > flux(n_q_points);
	
	std::vector< ForcingVectorType<n_components> > forcing(n_q_points);
	
	for (unsigned int q(0); q < n_q_points; ++q) {
		flux_matrix(u_theta[q], flux[q]);
		
		forcing_vector(u_theta[q], forcing[q]);
	}
	
	// Perform the assembly.
	
	// We have an outer loop through the components of the system
	// (all test functions phi_i), and
	// an inner loop over the quadrature points,
	// where we accumulate contributions to the $i$th residual $F_i$.
	// 
	// The general formula for this residual is given in the introduction and at
	// the top of this function.
	// We can, however, simplify it a bit taking into account that the $i$th
	// (vector-valued) test function $\mathbf{z}_i$ has in reality only a
	// single nonzero component!!!
	// 
	// With this, the residual term can be re-written as
	//   $F_i = \left(\frac{(\mathbf{w}_{n+1} - \mathbf{w}_n)_{\text{component\_i}}}{\delta t},
	//                (\mathbf{z}_i)_{\text{component\_i}}\right)_K$
	// 
	//   $-\sum_{d=1}^{\text{dim}} \left(\mathbf{F} (\tilde{\mathbf{w}})_{\text{component\_i},d},
	//    \frac{\partial(\mathbf{z}_i)_{\text{component\_i}}} {\partial x_d}\right)_K$
	// 
	//   $+ \sum_{d=1}^{\text{dim}} h^{\eta} \left(\frac{
	//      \partial \mathbf{w}_{\text{component\_i}}}{\partial x_d} ,
	//      \frac{\partial (\mathbf{z}_i)_{\text{component\_i}}}{\partial x_d} \right)_K$
	// 
	//   $-(\mathbf{G}(\tilde{\mathbf{w}} )_{\text{component\_i}},
	//     (\mathbf{z}_i)_{\text{component\_i}})_K$,
	// where integrals are understood to be evaluated through summation over
	// quadrature points.
	// 
	// We initially sum all contributions of the residual in the POSITIVE SENSE,
	// so that we DO NOT NEED to negative the Jacobian entries.
	// 
	// Then, when we sum into the <code>right_hand_side</code> vector,
	// we negate this residual.
	
	// AD residual variable.
	// residual value + derivative for test function i
	Sacado::Fad::DFad<double> F_i;
	
	const double h_diffusion_power =
		std::pow(fe_values.get_cell()->diameter(), streamline_diffusion_power);
	
	// loop over all test functions phi_i
	for (unsigned int i(0); i < fe_values.dofs_per_cell; ++i) {
		F_i = 0;
		
		const unsigned int c =
			fe_values.get_fe().system_to_component_index(i).first;
		
		for (unsigned int q(0); q < n_q_points; ++q) {
			
			const double JxW_q = fe_values.JxW(q);
			
			const double phi_component =
				fe_values.shape_value_component(i, q, c);
			
			const dealii::Tensor<1,dim> grad_phi_component =
				fe_values.shape_grad_component(i, q, c);
			
			F_i +=
				(1.0 / tau) * (u[q][c] - u_old[q][c]) *
				phi_component * JxW_q;
			
			for (unsigned int k(0); k < dim; ++k) {
				F_i -= flux[q][c][k] * grad_phi_component[k] * JxW_q;
			}
			
			for (unsigned int k(0); k < dim; ++k) {
				F_i +=
					h_diffusion_power *
					grad_u[q][c][k] * grad_phi_component[k] * JxW_q;
			}
			
			F_i -= forcing[q][c] * phi_component * JxW_q;
			
		} // for each quadrature point
		
		/// store residual_derivatives[k] = \f$ \frac{\partial F_i}{\partial z_k} \f$
		for (unsigned int k(0); k < dofs_per_cell; ++k) {
			residual_derivatives[k] = F_i.fastAccessDx(k);
		}
		
		// This information about the whole row of local dofs is then added to
		// the Trilinos matrix at once.
		system_matrix.add(
			dofs[i],      // matrix row
			dofs,         // matrix col entries
			residual_derivatives // values to be added
		);
		
		// subtract the residual from the right hand side.
		right_hand_side(dofs[i]) -= F_i.val();
		
	} // for i < dofs_per_cell
}


template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
assemble_face_term(
	const unsigned int face_no,
	const dealii::FEFaceValuesBase<dim> &fe_face_values,
	const dealii::FEFaceValuesBase<dim> &fe_face_values_neighbor,
	const std::vector<dealii::types::global_dof_index> &dofs,
	const std::vector<dealii::types::global_dof_index> &dofs_neighbor,
	const bool interior_face,
	const unsigned int &boundary_id,
	const double face_diameter
) {
	const unsigned int n_q_points = fe_face_values.n_quadrature_points;
	const unsigned int dofs_per_cell = fe_face_values.dofs_per_cell;
	
	std::vector<Sacado::Fad::DFad<double> > independent_local_dof_values(
		dofs_per_cell
	);
	
	std::vector<Sacado::Fad::DFad<double> > independent_neighbor_dof_values(
		interior_face ? dofs_per_cell : 0
	);
	
	const unsigned int n_independent_variables(
		interior_face ? 2 * dofs_per_cell : dofs_per_cell
	);
	
	for (unsigned int i(0); i < dofs_per_cell; ++i) {
		independent_local_dof_values[i] = current_solution(dofs[i]);
		independent_local_dof_values[i].diff(i, n_independent_variables);
	}
	
	if (interior_face) {
		for (unsigned int i(0); i < dofs_per_cell; ++i) {
			independent_neighbor_dof_values[i] =
				current_solution(dofs_neighbor[i]);
			
			independent_neighbor_dof_values[i].diff(
				i+dofs_per_cell,
				n_independent_variables
			);
		}
	}
	
	// Next, we need to define the values of the conservative variables
	// $\tilde {\mathbf u}$ on this side of the face ($\tilde {\mathbf u}^+$)
	// and on the opposite side ($\tilde {\mathbf u}^-$).
	std::vector< SolutionType<n_components> > u_plus(n_q_points);
	
	for (unsigned int q(0); q < n_q_points; ++q) {
		for (unsigned int i(0); i < dofs_per_cell; ++i) {
			const unsigned int c =
				fe_face_values.get_fe().system_to_component_index(i).first;
			
			u_plus[q][c] +=
				(theta * independent_local_dof_values[i]
				+ (1.-theta) * old_solution(dofs[i]))
				* fe_face_values.shape_value_component(i, q, c);
		}
	}
	
	
	std::vector< SolutionType<n_components> > u_minus(n_q_points);
	
	// compute u_minus on
	if (interior_face) {
		for (unsigned int q(0); q < n_q_points; ++q) {
			for (unsigned int i(0); i < dofs_per_cell; ++i) {
				const unsigned int c =
					fe_face_values_neighbor.get_fe().system_to_component_index(i).first;
				
				u_minus[q][c] +=
					( theta * independent_neighbor_dof_values[i]
					  + (1.-theta)
					  * old_solution(dofs_neighbor[i]) )
					* fe_face_values_neighbor.shape_value_component(i, q, c);
			}
		}
	}
	else {
		// boundary face
		
		enforce_boundary_conditions(
			u_plus,
			boundary_id,
			fe_face_values.get_quadrature_points(),
			fe_face_values.get_normal_vectors(),
			u_minus
		);
		
	}
	
	
	// Numerical normal flux of Lax-Friedrich's type with stabilisation:
	std::vector< FluxFunctionVectorType<n_components> > normal_fluxes(n_q_points);
	
	
	// TODO: stabilisation parameter parsing (step-33 default = 0 / not set)
// 	alpha = 0.; // default taken by the old step-33 implementation
// 	double alpha;
// 	switch (parameters.stabilization_kind) {
// 	case Parameters::Flux::constant:
// 		alpha = parameters.stabilization_value;
// 		break;
// 		
// 	case Parameters::Flux::mesh_dependent:
// 		alpha = face_diameter/(2.0*parameters.time_step);
// 		break;
// 		
// 	default:
// 		AssertThrow(
// 			false,
// 			dealii::ExcNotImplemented()
// 		);
// 	}
	
	// we use, for now, the mesh dependent variant
	double alpha = face_diameter/(2.0*tau);
	
	
	for (unsigned int q(0); q < n_q_points; ++q) {
		numerical_normal_flux(
			u_plus[q],
			u_minus[q],
			fe_face_values.normal_vector(q),
			alpha,
			normal_fluxes[q]
		);
	}
	
	
	// Assemble the face terms:
	
	std::vector<double> residual_derivatives(dofs_per_cell);
	
	// AD residual variable.
	// residual value + derivative for test function i
	Sacado::Fad::DFad<double> F_i;
	
	// loop over all test functions phi_i (i = local dof index)
	for (unsigned int i(0); i < fe_face_values.dofs_per_cell; ++i) {
		
		// if the test function phi_i has support on this face, then assemble:
		if (fe_face_values.get_fe().has_support_on_face(i, face_no) == true) {
			
			F_i = 0;
			
			const unsigned int c =
				fe_face_values.get_fe().system_to_component_index(i).first;
			
			for (unsigned int q(0); q < n_q_points; ++q) {
				F_i +=
					normal_fluxes[q][c] * fe_face_values.shape_value_component(i, q, c) *
					fe_face_values.JxW(q);
			}
			
			for (unsigned int k(0); k < dofs_per_cell; ++k) {
				residual_derivatives[k] = F_i.fastAccessDx(k);
			}
			
			system_matrix.add(
				dofs[i],
				dofs,
				residual_derivatives
			);
			
			if (interior_face) {
				for (unsigned int k(0); k < dofs_per_cell; ++k) {
					residual_derivatives[k] = F_i.fastAccessDx(dofs_per_cell+k);
				}
				
				system_matrix.add(
					dofs[i], dofs_neighbor,
					residual_derivatives
				);
			}
			
			right_hand_side(dofs[i]) -= F_i.val();
			
		} // test function has local support on this face
	} // loop test functions
}


/** ConservationLaw::solve
 *
 * Here, we actually solve the linear system, using either of Trilinos'
 * Aztec or Amesos linear solvers. The result of the computation will be
 * written into the argument vector passed to this function. The result is a
 * pair of number of iterations and the final linear residual.
 */
template<int dim, int n_components>
std::pair<unsigned int, double>
ConservationLaw<dim,n_components>::
solve(dealii::Vector<double> &newton_update) {
// 	switch (parameters.solver) {
// 		// TODO: make this a DTM++.core solver direct
// 		
// 		// If the parameter file specified that a direct solver shall be used,
// 		// then we'll get here. The process is straightforward, since deal.II
// 		// provides a wrapper class to the Amesos direct solver within
// 		// Trilinos. All we have to do is to create a solver control object
// 		// (which is just a dummy object here, since we won't perform any
// 		// iterations), and then create the direct solver object. When
// 		// actually doing the solve, note that we don't pass a
// 		// preconditioner. That wouldn't make much sense for a direct solver
// 		// anyway.  At the end we return the solver control statistics &mdash;
// 		// which will tell that no iterations have been performed and that the
// 		// final linear residual is zero, absent any better information that
// 		// may be provided here:
// 		case Parameters::Solver::direct: {
// 			dealii::SolverControl solver_control(1,0);
// 			
// 			TrilinosWrappers::SolverDirect direct(
// 				solver_control,
// 				parameters.output == Parameters::Solver::verbose
// 			);
// 			
// 			direct.solve(system_matrix, newton_update, right_hand_side);
// 			
// 			return std::pair<unsigned int, double> (
// 				solver_control.last_step(),
// 				solver_control.last_value()
// 			);
// 		}
// 		
// 		// TODO: make this a DTM++.core solver iterative GMRES/Precond.
// 		
// 		// Likewise, if we are to use an iterative solver, we use Aztec's GMRES
// 		// solver. We could use the Trilinos wrapper classes for iterative
// 		// solvers and preconditioners here as well, but we choose to use an
// 		// Aztec solver directly. For the given problem, Aztec's internal
// 		// preconditioner implementations are superior over the ones deal.II has
// 		// wrapper classes to, so we use ILU-T preconditioning within the
// 		// AztecOO solver and set a bunch of options that can be changed from
// 		// the parameter file.
// 		//
// 		// There are two more practicalities: Since we have built our right hand
// 		// side and solution vector as deal.II Vector objects (as opposed to the
// 		// matrix, which is a Trilinos object), we must hand the solvers
// 		// Trilinos Epetra vectors.  Luckily, they support the concept of a
// 		// 'view', so we just send in a pointer to our deal.II vectors. We have
// 		// to provide an Epetra_Map for the vector that sets the parallel
// 		// distribution, which is just a dummy object in serial. The easiest way
// 		// is to ask the matrix for its map, and we're going to be ready for
// 		// matrix-vector products with it.
// 		//
// 		// Secondly, the Aztec solver wants us to pass a Trilinos
// 		// Epetra_CrsMatrix in, not the deal.II wrapper class itself. So we
// 		// access to the actual Trilinos matrix in the Trilinos wrapper class by
// 		// the command trilinos_matrix(). Trilinos wants the matrix to be
// 		// non-constant, so we have to manually remove the constantness using a
// 		// const_cast.
// 		case Parameters::Solver::gmres: {
// 			// Set up TRILINOS Vectors and Trilinos GMRES
// 			
// 			Epetra_Vector x(
// 				Epetra_DataAccess::View,
// 				system_matrix.domain_partitioner(),
// 				newton_update.begin()
// 			);
// 			
// 			Epetra_Vector b(
// 				Epetra_DataAccess::View,
// 				system_matrix.range_partitioner(),
// 				right_hand_side.begin()
// 			);
// 			
// 			AztecOO solver;
// 			
// 			solver.SetAztecOption(
// 				AZ_output,
// 				(parameters.output == Parameters::Solver::quiet ? AZ_none : AZ_all)
// 			);
// 			
// 			solver.SetAztecOption(AZ_solver, AZ_gmres);
// 			
// 			solver.SetRHS(&b);
// 			solver.SetLHS(&x);
// 			
// 			solver.SetAztecOption(AZ_precond,         AZ_dom_decomp);
// 			solver.SetAztecOption(AZ_subdomain_solve, AZ_ilut);
// 			solver.SetAztecOption(AZ_overlap,         0);
// 			solver.SetAztecOption(AZ_reorder,         0);
// 			
// 			solver.SetAztecParam(AZ_drop,      parameters.ilut_drop);
// 			solver.SetAztecParam(AZ_ilut_fill, parameters.ilut_fill);
// 			solver.SetAztecParam(AZ_athresh,   parameters.ilut_atol);
// 			solver.SetAztecParam(AZ_rthresh,   parameters.ilut_rtol);
// 			
// 			solver.SetUserMatrix(
// 				const_cast<Epetra_CrsMatrix *> (&system_matrix.trilinos_matrix())
// 			);
// 			
// 			solver.Iterate(
// 				parameters.max_iterations,
// 				parameters.linear_residual
// 			);
// 			
// 			return std::pair<unsigned int, double> (
// 				solver.NumIters(),
// 				solver.TrueResidual()
// 			);
// 		}
// 	}
	
	
	// Set up TRILINOS Vectors and Trilinos GMRES
	
	Epetra_Vector x(
		Epetra_DataAccess::View,
		system_matrix.domain_partitioner(),
		newton_update.begin()
	);
	
	Epetra_Vector b(
		Epetra_DataAccess::View,
		system_matrix.range_partitioner(),
		right_hand_side.begin()
	);
	
	AztecOO solver;
	
	solver.SetAztecOption(
		AZ_output,
		AZ_none //(parameters.output == Parameters::Solver::quiet ? AZ_none : AZ_all)
	);
	
	solver.SetAztecOption(AZ_solver, AZ_gmres);
	
	solver.SetRHS(&b);
	solver.SetLHS(&x);
	
	solver.SetAztecOption(AZ_precond,         AZ_dom_decomp);
	solver.SetAztecOption(AZ_subdomain_solve, AZ_ilut);
	solver.SetAztecOption(AZ_overlap,         0);
	solver.SetAztecOption(AZ_reorder,         0);
	
	solver.SetAztecParam(AZ_drop,      1e-6); // parameters.ilut_drop);
	solver.SetAztecParam(AZ_ilut_fill, 1.5); //parameters.ilut_fill);
	solver.SetAztecParam(AZ_athresh,   1e-6); //parameters.ilut_atol);
	solver.SetAztecParam(AZ_rthresh,   1.0); //parameters.ilut_rtol);
	
	solver.SetUserMatrix(
		const_cast<Epetra_CrsMatrix *> (&system_matrix.trilinos_matrix())
	);
	
	solver.Iterate(
		100, //parameters.max_iterations,
		1e-10 // parameters.linear_residual
	);
	
	return std::pair<unsigned int, double> (
		solver.NumIters(),
		solver.TrueResidual()
	);
	
	// Throw an exception, in the case we reach this line.
	// -- something gone wrong --
	// Possibly, the solver type from the input parameter file is not provided
	// by this application.
	AssertThrow(false, dealii::ExcNotImplemented());
	
	return std::pair<unsigned int, double> (
		dealii::numbers::invalid_unsigned_int,
		std::numeric_limits<double>::quiet_NaN()
	);
}


/** ConservationLaw::refine_grid
 * Here, we use the refinement indicators computed before and refine the
 * mesh. At the beginning, we loop over all cells and mark those that we
 * think should be refined.
 */
template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
refine_grid(
	const dealii::Vector<double> &refinement_indicators
) {
	typename dealii::DoFHandler<dim>::active_cell_iterator cell = dof->begin_active();
	typename dealii::DoFHandler<dim>::active_cell_iterator endc = dof->end();
	
	for (unsigned int cell_no(0); cell != endc; ++cell, ++cell_no) {
		cell->clear_coarsen_flag();
		cell->clear_refine_flag();
		
		if ((cell->level() < static_cast<int>(shock_levels)) &&
			(std::fabs(refinement_indicators(cell_no)) > shock_value)) {
			cell->set_refine_flag();
		}
		else if ((cell->level() > 0) &&
				 (std::fabs(refinement_indicators(cell_no)) < 0.75*shock_value)) {
			cell->set_coarsen_flag();
		}
	}
	
	// Then we need to transfer the various solution vectors from the old to
	// the new grid while we do the refinement.
	// 
	// The SolutionTransfer class is used here;
	//   it has a fairly extensive documentation, including
	//   examples, so we won't comment much on the following code.
	// 
	// The last three lines simply re-set the sizes of some other vectors to the
	// new correct size.
	
	std::vector<dealii::Vector<double> > transfer_in;
	
	transfer_in.push_back(old_solution);
	transfer_in.push_back(predictor);
	
	triangulation.prepare_coarsening_and_refinement();
	
	dealii::SolutionTransfer<dim> soltrans(*dof);
	soltrans.prepare_for_coarsening_and_refinement(transfer_in);
	
	triangulation.execute_coarsening_and_refinement ();
	
	dof->clear();
	dof->distribute_dofs (fe);
	
	std::vector<dealii::Vector<double> > transfer_out;
	{
		const dealii::types::global_dof_index n_dofs(dof->n_dofs());
		dealii::Vector<double> vec(n_dofs);
		
		transfer_out.push_back(vec); // new_old_solution
		transfer_out.push_back(vec); // new_predictor
	}
	
	soltrans.interpolate(transfer_in, transfer_out);
	
	old_solution.reinit(transfer_out[0].size());
	old_solution = transfer_out[0];
	
	predictor.reinit(transfer_out[1].size());
	predictor = transfer_out[1];
	
	current_solution.reinit(dof->n_dofs());
	current_solution = old_solution;
	
	right_hand_side.reinit(dof->n_dofs());
}



/** ConservationLaw::run
 * 
 * This function contains the top-level logic of this program:
 * initialization, the time loop, and the inner Newton iteration.
 * 
 * At the beginning, we read the mesh file specified by the parameter file,
 * setup the DoFHandler and various vectors, and then interpolate the given
 * initial conditions on this mesh. We then perform a number of mesh
 * refinements, based on the initial conditions, to obtain a mesh that is
 * already well adapted to the starting solution. At the end of this
 * process, we output the initial solution.
 */
template<int dim, int n_components>
void
ConservationLaw<dim,n_components>::
run() {
	AssertThrow(input_parameters_set, dealii::ExcNotInitialized());
	
	// Read in the given mesh description given by the input parameter value.
	// The given mesh description file (*.inp) must be in UCD formatted style!
	{
		dealii::GridIn<dim> grid_in;
		grid_in.attach_triangulation(triangulation);
		
		std::ifstream input_file(mesh_filename);
		
		AssertThrow(
			input_file,
			dealii::ExcFileNotOpen(mesh_filename.c_str())
		);
		
		grid_in.read_ucd(input_file);
	}
	
	dof->clear();
	dof->distribute_dofs(fe);
	
	old_solution.reinit(dof->n_dofs());
	current_solution.reinit(dof->n_dofs());
	predictor.reinit(dof->n_dofs());
	right_hand_side.reinit(dof->n_dofs());
	
	setup_system();
	
	dealii::VectorTools::interpolate(
		*dof,
		*u_0,
		old_solution
	);
	
	current_solution = old_solution;
	predictor = old_solution;
	
	if (shock_caputuring_enabled) {
		Assert(
			h_adaptivity_enabled,
			dealii::ExcMessage("You enabled Shock Capturing but disabled h-adaptivity!")
		);
		
		for (unsigned int i(0); i < shock_levels; ++i) {
			dealii::Vector<double> refinement_indicators(
				triangulation.n_active_cells()
			);
			
			compute_refinement_indicators(
				*dof,
				mapping,
				predictor,
				refinement_indicators
			);
			
			refine_grid(refinement_indicators);
			
			setup_system();
			
			dealii::VectorTools::interpolate(
				*dof,
				*u_0,
				old_solution
			);
			
			current_solution = old_solution;
			predictor = old_solution;
		}
	}
	
	output_results ();
	
	////////////////////////////////////////////////////////////////////////////
	// Time Marching Loop
	// 
	
	// We then enter into the main time stepping loop. At the top we simply
	// output some status information so one can keep track of where a
	// computation is, as well as the header for a table that indicates
	// progress of the nonlinear inner iteration:
	
	dealii::Vector<double> newton_update(dof->n_dofs());
	
	time = t0;
	
	double next_output = time + param_output_step;
	
	predictor = old_solution;
	
	while (time < T) {
		std::cout
		<< "T=" << time << " , tau_n = " << tau << std::endl
		<< "   Number of active cells:       "
		<< triangulation.n_active_cells() << std::endl
		<< "   Number of degrees of freedom: "
		<< dof->n_dofs() << std::endl
		<< std::endl;
		
		std::cout
		<< "   NonLin Res     Lin Iter       Lin Res" << std::endl
		<< "   _____________________________________" << std::endl;
		
		// Then comes the inner Newton iteration to solve the nonlinear
		// problem in each time step. The way it works is to reset matrix and
		// right hand side to zero, then assemble the linear system. If the
		// norm of the right hand side is small enough, then we declare that
		// the Newton iteration has converged. Otherwise, we solve the linear
		// system, update the current solution with the Newton increment, and
		// output convergence information. At the end, we check that the
		// number of Newton iterations is not beyond a limit of 10 -- if it
		// is, it appears likely that iterations are diverging and further
		// iterations would do no good. If that happens, we throw an exception
		// that will be caught in <code>main()</code> with status information
		// being displayed before the program aborts.
		//
		// Note that the way we write the AssertThrow macro below is by and
		// large equivalent to writing something like <code>if (!(nonlin_iter
		// @<= 10)) throw ExcMessage ("No convergence in nonlinear
		// solver");</code>. The only significant difference is that
		// AssertThrow also makes sure that the exception being thrown carries
		// with it information about the location (file name and line number)
		// where it was generated. This is not overly critical here, because
		// there is only a single place where this sort of exception can
		// happen; however, it is generally a very useful tool when one wants
		// to find out where an error occurred.
		
		unsigned int nonlin_iter = 0;
		
		current_solution = predictor;
		
		while (true) {
			system_matrix = 0;
			
			right_hand_side = 0;
			
			assemble_system();
			
			const double res_norm = right_hand_side.l2_norm();
			
			if (std::fabs(res_norm) < tol_newton_solver) {
				if (!MyPID)
					std::printf("   %-16.3e (converged)\n\n", res_norm);
				
				// break while loop (Newton iteration (converged))
				break;
			}
			else {
				newton_update = 0;
				
				std::pair<unsigned int, double> convergence
				= solve(newton_update);
				
				current_solution += newton_update;
				
				if (!MyPID)
					std::printf(
						"   %-16.3e %04d        %-5.2e\n",
						res_norm, convergence.first, convergence.second
					);
			}
			
			++nonlin_iter;
			
			AssertThrow(
				nonlin_iter <= max_iter_newton_solver,
				dealii::ExcMessage("No convergence in nonlinear solver")
			);
		} // NEWTON iteration
		
		// We only get to this point if the Newton iteration has converged, so
		// do various post convergence tasks here:
		//
		// First, we update the time and produce graphical output if so
		// desired.
		
		time += tau;
		
		// TODO:
		output_results();
// 		if (param_output_step < 0) {
// 			output_results();
// 		}
// 		else if (time >= next_output) {
// 			output_results();
// 			next_output += param_output_step;
// 		}
		
		// Then we update a predictor for the solution at the next
		// time step by approximating
		//   $\mathbf w^{n+1} \approx \mathbf w^n +
		//     \delta t \frac{\partial \mathbf w}{\partial t} \approx \mathbf w^n
		//     + \delta t \; \frac{\mathbf w^n-\mathbf w^{n-1}}{\delta t} = 2
		//     \mathbf w^n - \mathbf w^{n-1}$
		// to try and make adaptivity work better.
		// 
		// The idea is to try and refine ahead of a front, rather
		// than stepping into a coarse set of elements and smearing the
		// old_solution.
		// 
		// This simple time extrapolator does the job.
		// With this, we then refine the mesh if so desired by the user, and
		// finally continue on with the next time step.
		
		predictor = current_solution;
		predictor.sadd(2.0, -1.0, old_solution);
		
		old_solution = current_solution;
		
		if (h_adaptivity_enabled) {
			dealii::Vector<double> refinement_indicators(
				triangulation.n_active_cells()
			);
			
			compute_refinement_indicators(
				*dof,
				mapping,
				predictor,
				refinement_indicators
			);
			
			refine_grid(refinement_indicators);
			
			setup_system();
			
			newton_update.reinit(dof->n_dofs());
		}
		
		// time step size control
		if (nonlin_iter < 3) {
			tau *= 2.;
		}
		
		if (nonlin_iter >= 3) {
			tau /= 4.;
		}
		
		if (tau < tau_min) {
			std::cout << "Step Size Control: underrun, using tau_min" << std::endl;
			tau = tau_min;
		}
		else if (tau > tau_max) {
			std::cout << "Step Size Control: overflow, using tau_max" << std::endl;
			tau = tau_max;
		}
		
	} // while (time marching loop)
	
	// cleanup
	dof->clear();
	triangulation.clear();
}

}} // namespace

#include "ConservationLaw.inst.in"

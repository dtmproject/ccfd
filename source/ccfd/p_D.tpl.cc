/**
 * @file p_D.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-23, UK
 *
 * @brief Boundary value function \f$ p_D \f$.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __p_D_tpl_cc
#define __p_D_tpl_cc

// Project includes
#include <DTM++/core/base/Function.tpl.hh>

// MPI includes

// DEAL.II includes


// C++ includes

namespace ccfd {
namespace euler {

template<int dim>
class p_D : public DTM::core::Function<dim,1> {
public:
	p_D() = default;
	virtual ~p_D() = default;

	virtual void vector_value(
		const dealii::Point<dim> &,
		dealii::Vector<double> &
	) const;
};


template<int dim>
void
p_D<dim>::
vector_value(
	const dealii::Point<dim> &x,
	dealii::Vector<double> &y
) const {
	Assert(dim == 2, dealii::ExcNotImplemented());
	
// 	// get time
// 	const double t(get_time());
	
	/// pressure boundary value \f$ \p_D \f$
	y(0) = (1.5 - x[1]);
	
	return;
}


template class p_D<2>;

}} // namespace

#endif

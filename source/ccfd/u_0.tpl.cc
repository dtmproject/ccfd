/**
 * @file u_0.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-23, UK
 *
 * @brief Initial value function
 *        \f$ \boldsymbol u_0 = (\rho_0, \rho_0 v_1^0, \rho_0 v_2^0, E_0)^T\f$.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __u_0_tpl_cc
#define __u_0_tpl_cc

// Project includes
#include <DTM++/core/base/Function.tpl.hh>

// MPI includes

// DEAL.II includes


// C++ includes

namespace ccfd {
namespace euler {

template<int dim, int n_components>
class u_0 : public DTM::core::Function<dim,n_components> {
public:
	u_0() = default;
	virtual ~u_0() = default;

	virtual void vector_value(
		const dealii::Point<dim> &,
		dealii::Vector<double> &
	) const;
};


template<int dim, int n_components>
void
u_0<dim,n_components>::
vector_value(
	const dealii::Point<dim> &x,
	dealii::Vector<double> &y
) const {
	Assert(dim == 2, dealii::ExcNotImplemented());
	Assert(n_components == 4, dealii::ExcNotImplemented());
	
// 	// get time
// 	const double t(get_time());
	
	/// density \f$ \rho_0 \f$
	if ( (x[0] < -0.7) && (x[1] > 0.3) && (x[1] < 0.45) ) {
		y(0) = 10.;
	} else {
		y(0) = 1.;
	}
	
	// check, if the density is zero (this leads to a divide by zero in the code)
	Assert(y(0) != 0., dealii::ExcDivideByZero());
	
	
	/// momentum density component \f$ \rho_0 v_1^0 \f$
	y(1) = 0;
	
	/// momentum density component \f$ \rho_0 v_2^0 \f$
	y(2) = 0;
	
	
	/// total energy density
	/// \f$ E_0 = \frac{p_0}{(\gamma - 1)}
	///           + \frac{1}{2}\, \rho_0 | \boldsymbol v |^2) \f$
	y(3) = (1.5 - x[1])/(0.4);
	
	return;
}


template class u_0<2,4>;

}} // namespace

#endif

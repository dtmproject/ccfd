/**
 * @file v_D.tpl.cc
 * @author Uwe Koecher (UK)
 * @date 2015-02-24, UK
 *
 * @brief Boundary value function \f$ \boldsymbol v_D \f$.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++.                                               */
/*                                                                            */
/*  DTM++ is free software: you can redistribute it and/or modify             */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++ is distributed in the hope that it will be useful,                  */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++.  If not, see <http://www.gnu.org/licenses/>.            */

#ifndef __v_D_tpl_cc
#define __v_D_tpl_cc

// Project includes
#include <DTM++/core/base/TensorFunction.tpl.hh>

// MPI includes

// DEAL.II includes
#include <deal.II/base/tensor.h>


// C++ includes

namespace ccfd {
namespace euler {

template<int dim>
class v_D : public DTM::core::TensorFunction<1,dim,double> {
public:
	v_D() = default;
	virtual ~v_D() = default;
	
	/// get value from a function evaluation
	virtual
	dealii::Tensor<1,dim,double>
	value(
		const dealii::Point<dim,double> &x
	) const;
};


template<int dim>
dealii::Tensor<1,dim,double>
v_D<dim>::
value(
	const dealii::Point<dim,double> &x
) const {
	dealii::Tensor<1,dim,double> y;
	y=0;
	
	return y;
}



template class v_D<2>;

}} // namespace

#endif

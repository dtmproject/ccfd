/**
 * @file   EulerEquations.tpl.cc
 * @author Uwe Koecher (UK)
 * @date   2015-02-13, introducing hierachical model, UK
 * @date   2015-02-10, UK
 *
 * @brief DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/CCFD (Comput. Compressible Fluid Dynamics).    */
/*                                                                            */
/*  DTM++/CCFD is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/CCFD is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/CCFD.   If not, see <http://www.gnu.org/licenses/>.      */

/* ORIGIN of this code was (partly) deal.II/example/step-33/step-33.cc:
 * ---------------------------------------------------------------------
 *
 * Copyright (C) 2007 - 2015 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE at
 * the top level of the deal.II distribution.
 *
 * ---------------------------------------------------------------------
 *
 * Author: David Neckels, Boulder, Colorado, 2007, 2008
 */

// TODO:
#include "u_0.tpl.cc"
#include "rho_D.tpl.cc"
#include "v_D.tpl.cc"
#include "p_D.tpl.cc"

// PROJECT includes
#include <ccfd/EulerEquations.tpl.hh>
#include <ccfd/EulerEquationsDataPostprocessor.tpl.hh>

// DEAL.II includes
#include <deal.II/base/mpi.h>
#include <deal.II/base/parameter_handler.h>
#include <deal.II/base/table.h>
#include <deal.II/base/utilities.h>

#include <deal.II/dofs/dof_tools.h>

#include <deal.II/fe/fe_q.h>

#include <deal.II/grid/grid_in.h>

#include <deal.II/lac/solver_control.h>
#include <deal.II/lac/trilinos_solver.h>

#include <deal.II/numerics/data_out.h>
#include <deal.II/numerics/solution_transfer.h>

// TRILIOS includes
#include <Sacado.hpp>

// C++ includes
#include <cmath>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

namespace ccfd {
namespace euler {

template<int dim, int n_components>
EulerEquations<dim,n_components>::
EulerEquations() :
	gamma(1.4), // ratio of specific heat for a diatomic gas
	gamma_minus_one(0.4)
{
	Assert(n_components == dim+2, dealii::ExcNotImplemented());
	
	// initialize component_names
	component_names.push_back("density");
	for (unsigned int k(0); k < dim; ++k) {
		component_names.push_back("momentum_density");
	}
	component_names.push_back("total_energy_density");
	
	// initialize data component interpretation (dci)
	dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
	for (unsigned int k(0); k < dim; ++k) {
		dci.push_back(
			dealii::DataComponentInterpretation::component_is_part_of_vector
		);
	}
	dci.push_back(dealii::DataComponentInterpretation::component_is_scalar);
	
	// initialize gravity_vector
	gravity_vector(dim-1) = -1.0;
	
	// TODO load initial value function
	// set initial value function (with n_components)
	ConservationLaw<dim,n_components>::u_0 =
		std::make_shared< ccfd::euler::u_0<dim,n_components> > ();
	
	// TODO load prescribed boundary value functions
	// set prescribed boundary value functions
// 	rho_D = std::make_shared< dealii::ConstantFunction<dim> > (1.);
	rho_D = std::make_shared< ccfd::euler::rho_D<dim> > ();
	
// 	v_D = std::make_shared< dealii::ZeroTensorFunction<1,dim> > ();
	v_D = std::make_shared< ccfd::euler::v_D<dim> > ();
	
// 	p_D = std::make_shared< dealii::ConstantFunction<dim> > (1.);
	p_D = std::make_shared< ccfd::euler::p_D<dim> > ();
}


template<int dim, int n_components>
void
EulerEquations<dim,n_components>::
set_input_parameters(dealii::ParameterHandler &parameters) {
	ConservationLaw<dim,n_components>::set_input_parameters(parameters);
	
	unsigned int output_quantities(0);
	
	parameters.enter_subsection("Output Quantities"); {
		if (parameters.get_bool("density"))
			output_quantities += OutputQuantities::density;
		
		if (parameters.get_bool("momentum"))
			output_quantities += OutputQuantities::momentum;
		
		if (parameters.get_bool("velocity"))
			output_quantities += OutputQuantities::velocity;
		
		if (parameters.get_bool("pressure"))
			output_quantities += OutputQuantities::pressure;
		
		if (parameters.get_bool("total energy per unit volume"))
			output_quantities += OutputQuantities::total_energy_volume;
		
		if (parameters.get_bool("total energy per unit mass"))
			output_quantities += OutputQuantities::total_energy_mass;
	}
	parameters.leave_subsection();
	
	////////////////////////////////////////////////////////////////////////////
	
	// load DataPostprocessor
	data_postprocessor = std::make_shared<
		ccfd::euler::DataPostprocessor<dim,n_components> > (
		output_quantities,
		density_component,
		first_momentum_component,
		energy_component,
		gamma_minus_one
	);
	
	// load and initialise DTM++.core data_out objects
	data_out_postprocessed_solution = std::make_shared<
		DTM::core::DataOutput< dim,dealii::Vector<double> > >();
	
	data_out_postprocessed_solution->set_output_format(
		DTM::core::DataOutput< dim,dealii::Vector<double> >::DataFormat::HDF5_XDMF
	);
	
	data_out_postprocessed_solution->set_DoF_data(
		this->dof,
		nullptr,
		nullptr
	);
	data_out_postprocessed_solution->set_data_output_patches(this->p);
	data_out_postprocessed_solution->set_setw(4);
}


template<int dim, int n_components>
std::vector<std::string>
EulerEquations<dim,n_components>::
get_component_names() const {
	return component_names;
}


template<int dim, int n_components>
std::vector<dealii::DataComponentInterpretation::DataComponentInterpretation>
EulerEquations<dim,n_components>::
get_dci() const {
	return dci;
}


template<int dim, int n_components>
void
EulerEquations<dim,n_components>::
flux_matrix(
	const SolutionType<n_components> &u,
	FluxMatrixType<n_components,dim> &FluxMatrix) const {
	// pressure p from u
	const SolutionComponentType p = pressure(u);
	
	// compute FluxMatrix[RowIndex][ColIndex]
	
	for (unsigned i(0); i < dim; ++i) {
		for (unsigned int j(0); j < dim; ++j) {
			Assert(u[density_component] != 0., dealii::ExcDivideByZero());
			
			FluxMatrix[first_momentum_component+i][j] =
				u[first_momentum_component+j] * u[first_momentum_component+i]
				/ u[density_component];
		}
		
		FluxMatrix[first_momentum_component+i][i] += p;
	}
	
	for (unsigned int j(0); j < dim; ++j) {
		Assert(u[density_component] != 0., dealii::ExcDivideByZero());
		
		FluxMatrix[density_component][j] = u[first_momentum_component+j];
		
		FluxMatrix[energy_component][j] =
			(u[energy_component] + p) *
			u[first_momentum_component+j] / u[density_component];
	}
}


template<int dim, int n_components>
void
EulerEquations<dim,n_components>::
forcing_vector(
	const SolutionType<n_components> &u,
	ForcingVectorType<n_components> &ForcingVector) const {
	
	ForcingVector[density_component] = 0.;
	
	for (unsigned int i(0); i < dim; ++i) {
		ForcingVector[first_momentum_component+i] =
			gravity_vector[i] * u[density_component];
	}
	
	ForcingVector[energy_component] = 0;
	for (unsigned int j(0); j < dim; ++j) {
		ForcingVector[energy_component] +=
			gravity_vector[j] * u[first_momentum_component+j];
	}
}


template<int dim, int n_components>
SolutionComponentType
EulerEquations<dim,n_components>::
kinetic_energy(const SolutionType<n_components> &u) const {
	Assert(u[density_component] != 0., dealii::ExcDivideByZero());
	SolutionComponentType kinetic_energy = 0;
	
	for (unsigned int k(0); k < dim; ++k) {
		kinetic_energy +=
			u[first_momentum_component+k] * u[first_momentum_component+k];
	}
	
	kinetic_energy /= (2. * u[density_component]);
	
	return kinetic_energy;
}


template<int dim, int n_components>
SolutionComponentType
EulerEquations<dim,n_components>::
pressure(const SolutionType<n_components> &u) const {
	return (
		(gamma - 1.) *
		(u[energy_component] - kinetic_energy(u) )
	);
}


template<int dim, int n_components>
void
EulerEquations<dim,n_components>::
compute_refinement_indicators(
	const dealii::DoFHandler<dim> &dof,
	const dealii::Mapping<dim> &mapping,
	const dealii::Vector<double> &solution,
	dealii::Vector<double> &refinement_indicators) const {
	
	const unsigned int dofs_per_cell(dof.get_fe().dofs_per_cell);
	
	std::vector<dealii::types::global_dof_index> dofs(dofs_per_cell);
	
	// Mid-point quadrature rule in a d-dimensional style;
	// contains only one quadrature point in the geometric center.
	const dealii::QMidpoint<dim> quadrature;
	
	const dealii::UpdateFlags uflags(dealii::update_gradients);
	
	dealii::FEValues<dim> fe_values(
		mapping,
		dof.get_fe(),
		quadrature,
		uflags
	);
	
	// Data structure for gradient of the numerical solution at the quadrature
	// points
	std::vector< std::vector< dealii::Tensor<1,dim> > > grad_u(
		quadrature.size(),
		std::vector< dealii::Tensor<1,dim> > (n_components)
	);
	
	auto cell = dof.begin_active();
	auto endc = dof.end();
	
	for (unsigned int cell_no(0); cell != endc; ++cell, ++cell_no) {
		fe_values.reinit(cell);
		fe_values.get_function_gradients(solution, grad_u);
		
		refinement_indicators(cell_no) = std::log(
			1. + std::sqrt(grad_u[0][density_component] * grad_u[0][density_component])
		);
	}
}


template<int dim, int n_components>
void
EulerEquations<dim,n_components>::
enforce_boundary_conditions(
	const std::vector< SolutionType<n_components> > &u_plus,
	const unsigned int &id,
	const std::vector< dealii::Point<dim> > &quadrature_points,
	const std::vector< dealii::Point<dim> > &normal_vectors,
	std::vector< SolutionType<n_components> > &u_minus
) const {
	const boundary_id boundary_id_value = static_cast<boundary_id> (id);
	const unsigned int n_q_points(quadrature_points.size());
	
	////////////////////////////////////////////////////////////////////////////
	// fetch prescribed boundary values if necessary
	std::vector< double > rho_D_values;
	std::vector< dealii::Tensor<1,dim> > v_D_values;
	std::vector< double > p_D_values;
	
	// Density and velocity prescribed boundaries
	if ((boundary_id_value == boundary_id::inflow_supersonic) ||
		(boundary_id_value == boundary_id::inflow_subsonic) ) {
		rho_D_values.resize(n_q_points);
		v_D_values.resize(n_q_points);
		
		rho_D->value_list(quadrature_points, rho_D_values);
		v_D->value_list(quadrature_points, v_D_values);
	}
	
	// Pressure prescribed boundaries
	if ((boundary_id_value == boundary_id::inflow_supersonic) ||
		(boundary_id_value == boundary_id::outflow_subsonic)) {
		p_D_values.resize(n_q_points);
		
		p_D->value_list(quadrature_points, p_D_values);
	}
	
	////////////////////////////////////////////////////////////////////////////
	// enforce boundary values
	switch (boundary_id_value) {
	case boundary_id::inflow_supersonic: {
		Assert(rho_D_values.size() == n_q_points, dealii::ExcNotInitialized());
		Assert(v_D_values.size() == n_q_points, dealii::ExcNotInitialized());
		Assert(p_D_values.size() == n_q_points, dealii::ExcNotInitialized());
		
		for (unsigned int q(0); q < n_q_points; ++q) {
			u_minus[q][density_component] = rho_D_values[q];
			
			for (unsigned int k(0); k < dim; ++k) {
				u_minus[q][first_momentum_component+k] =
					rho_D_values[q] * v_D_values[q][k];
			}
			
			u_minus[q][energy_component] =
				p_D_values[q]/(gamma_minus_one) +
				0.5 * rho_D_values[q] * v_D_values[q] * v_D_values[q];
		}
		return;
	}
	
	case boundary_id::inflow_subsonic: {
		Assert(rho_D_values.size() == n_q_points, dealii::ExcNotInitialized());
		Assert(v_D_values.size() == n_q_points, dealii::ExcNotInitialized());
		
		std::vector< SolutionComponentType > p_plus(n_q_points);
		for (unsigned int q(0); q < n_q_points; ++q) {
			p_plus[q] = pressure(u_plus[q]);
		}
		
		Assert(p_plus.size() == n_q_points, dealii::ExcNotInitialized());
		
		for (unsigned int q(0); q < n_q_points; ++q) {
			u_minus[q][density_component] = rho_D_values[q];
			
			for (unsigned int k(0); k < dim; ++k) {
				u_minus[q][first_momentum_component+k] =
					rho_D_values[q] * v_D_values[q][k];
			}
			
			u_minus[q][energy_component] =
				p_plus[q]/(gamma_minus_one) +
				0.5 * rho_D_values[q] * v_D_values[q] * v_D_values[q];
		}
		return;
	}
	
	case boundary_id::outflow_supersonic: {
		u_minus = u_plus;
		
		return;
	}
	
	case boundary_id::outflow_subsonic: {
		Assert(p_D_values.size() == n_q_points, dealii::ExcNotInitialized());
		
		for (unsigned int q(0); q < n_q_points; ++q) {
			u_minus[q][density_component] = u_plus[q][density_component];
			
			u_minus[q][energy_component] = 0;
			
			for (unsigned int k(0); k < dim; ++k) {
				u_minus[q][first_momentum_component+k] =
					u_plus[q][first_momentum_component+k];
				
				u_minus[q][energy_component] +=
					u_plus[q][first_momentum_component+k] *
					u_plus[q][first_momentum_component+k];
			}
			
			u_minus[q][energy_component] /= (2. * u_plus[q][density_component]);
			u_minus[q][energy_component] += p_D_values[q]/(gamma_minus_one);
		}
		break;
	}
	
	case boundary_id::solid_impermeable: {
		
		SolutionComponentType u_plus_dot_n;
		
		for (unsigned int q(0); q < n_q_points; ++q) {
			u_minus[q][density_component] = u_plus[q][density_component];
			
			u_plus_dot_n = 0;
			for (unsigned int k(0); k < dim; ++k) {
				u_plus_dot_n +=
					u_plus[q][first_momentum_component+k] *
					normal_vectors[q][k];
			}
			
			for (unsigned int k(0); k < dim; ++k) {
				u_minus[q][first_momentum_component+k] =
					u_plus[q][first_momentum_component+k]
					- 2. * u_plus_dot_n * normal_vectors[q][k];
			}
			
			u_minus[q][energy_component] = u_plus[q][energy_component];
		}
		return;
	}
	
	default:
		AssertThrow(false, dealii::ExcNotImplemented());
	} // switch
} // enforce_boundary_conditions


template<int dim, int n_components>
void
EulerEquations<dim,n_components>::
output_results() {
	data_out_postprocessed_solution->write(
		"postprocessed_solution",
		ConservationLaw<dim,n_components>::current_solution,
		data_postprocessor,
		ConservationLaw<dim,n_components>::time
	);
	
	data_out_postprocessed_solution->increment_data_file_counter();
	data_out_postprocessed_solution->increment_mesh_file_counter();
}



}} // namespace

#include "EulerEquations.inst.in"

/**
 * @file   Parameters.tpl.cc
 * @author Uwe Koecher (UK)
 * @date   2015-02-27, UK
 * @date   2015-02-17, (ParametersSolver) UK
 *
 * @brief DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/CCFD (Comput. Compressible Fluid Dynamics).    */
/*                                                                            */
/*  DTM++/CCFD is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/CCFD is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/CCFD.   If not, see <http://www.gnu.org/licenses/>.      */


// PROJECT includes
#include <ccfd/Parameters.tpl.hh>


// DEAL.II includes
#include <deal.II/base/parameter_handler.h>

// C++ includes

namespace ccfd {
namespace parameters {

Euler::
Euler() {
	enter_subsection("Time Integration"); {
		declare_entry(
			"initial time step size",
			"0.01",
			dealii::Patterns::Double(),
			"initial time step size"
		);
		
		declare_entry(
			"minimal time step size",
			"1e-10",
			dealii::Patterns::Double(),
			"minimal time step size"
		);
		
		declare_entry(
			"maximal time step size",
			"1e-1",
			dealii::Patterns::Double(),
			"maximal time step size"
		);
		
		declare_entry(
			"theta",
			"0.5",
			dealii::Patterns::Double(),
			"fractional-step theta parameter"
		);
		
		declare_entry(
			"initial time",
			"0.",
			dealii::Patterns::Double(),
			"initial time t0"
		);
		
		declare_entry(
			"final time",
			"0.",
			dealii::Patterns::Double(),
			"final time T"
		);
	}
	leave_subsection();
	
	enter_subsection("Output Quantities");
		declare_entry(
			"density",
			"true",
			dealii::Patterns::Bool(),
			"output density solution component"
		);
		
		declare_entry(
			"momentum",
			"true",
			dealii::Patterns::Bool(),
			"output momentum solution components"
		);
		
		declare_entry(
			"velocity",
			"false",
			dealii::Patterns::Bool(),
			"output velocity"
		);
		
		declare_entry(
			"pressure",
			"true",
			dealii::Patterns::Bool(),
			"output pressure"
		);
		
		declare_entry(
			"total energy per unit volume",
			"true",
			dealii::Patterns::Bool(),
			"output total energy per unit volume solution component"
		);
		
		declare_entry(
			"total energy per unit mass",
			"false",
			dealii::Patterns::Bool(),
			"output total energy per unit mass solution component"
		);
	leave_subsection();
	
	enter_subsection("Grid"); {
		declare_entry(
			"grid in filename",
			"./input/grid.ucd",
			dealii::Patterns::Anything(),
			"filename of the mesh which can be read in with dealii::GridIn"
		);
		
		declare_entry(
			"enable h-adaptivity",
			"false",
			dealii::Patterns::Bool(),
			"allow h-adaptivity (refinement and coarsening) of the grid"
		);
	}
	leave_subsection();
	
	enter_subsection("Shock Capturing"); {
		declare_entry(
			"enable shock capturing",
			"false",
			dealii::Patterns::Bool(),
			"enables shock capturing"
		);
		
		declare_entry(
			"shock levels",
			"0.",
			dealii::Patterns::Double(),
			"number of shock levels"
		);
		
		declare_entry(
			"shock value",
			"0.",
			dealii::Patterns::Double(),
			"shock value"
		);
	}
	leave_subsection();
	
	enter_subsection("Streamline Diffusion"); {
		declare_entry(
			"streamline diffusion power",
			"2.0",
			dealii::Patterns::Double(),
			"value of the power of h (mesh size) for the artificial streamline diffusion"
		);
	}
	leave_subsection();
	
}

}} // namespace

#include "Parameters.inst.in"

/**
 * @file   EulerEquationsDataPostprocessor.tpl.cc
 * @author Uwe Koecher (UK)
 * @date   2015-02-24, UK
 *
 * @brief DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
 */

/*  Copyright (C) 2012-2015 by Uwe Koecher                                    */
/*                                                                            */
/*  This file is part of DTM++/CCFD (Comput. Compressible Fluid Dynamics).    */
/*                                                                            */
/*  DTM++/CCFD is free software: you can redistribute it and/or modify        */
/*  it under the terms of the GNU Lesser General Public License as            */
/*  published by the Free Software Foundation, either                         */
/*  version 3 of the License, or (at your option) any later version.          */
/*                                                                            */
/*  DTM++/CCFD is distributed in the hope that it will be useful,             */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of            */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             */
/*  GNU Lesser General Public License for more details.                       */
/*                                                                            */
/*  You should have received a copy of the GNU Lesser General Public License  */
/*  along with DTM++/CCFD.   If not, see <http://www.gnu.org/licenses/>.      */


// PROJECT includes
#include <ccfd/EulerEquationsDataPostprocessor.tpl.hh>

// DEAL.II includes
#include <deal.II/base/exceptions.h>


// C++ includes


namespace ccfd {
namespace euler {


template<int dim, int n_components>
void
DataPostprocessor<dim,n_components>::
compute_derived_quantities_vector(
	const std::vector< dealii::Vector<double> > &uh,
	const std::vector< std::vector< dealii::Tensor<1,dim> > > &,
	const std::vector< std::vector< dealii::Tensor<2,dim> > > &,
	const std::vector< dealii::Point<dim> > &,
	const std::vector< dealii::Point<dim> > &,
	std::vector< dealii::Vector<double> > &postprocessed_quantities
) const {
	Assert(
		uh.size() != 0,
		dealii::ExcEmptyObject()
	);
	const unsigned int n_q_points(uh.size());
	
	Assert(
		uh[0].size() == n_components,
		dealii::ExcInternalError()
	);
	
	Assert(
		postprocessed_quantities.size() == n_q_points,
		dealii::ExcInternalError()
	);
	
	Assert(
		postprocessed_quantities[0].size() == internal_n_components,
		dealii::ExcInternalError()
	);
	
	
	for (unsigned int q(0); q < n_q_points; ++q) {
		// store density
		if (output_quantities & OutputQuantities::density) {
			postprocessed_quantities[q][internal_density_component] = uh[q][density_component];
		}
		
		// store momentum components
		if (output_quantities & OutputQuantities::momentum) {
			for (unsigned int k(0); k < dim; ++k) {
				postprocessed_quantities[q][internal_first_momentum_component+k] =
					uh[q][first_momentum_component+k];
			}
		}
		
		// compute velocity components
		if (output_quantities & OutputQuantities::velocity) {
			for (unsigned int k(0); k < dim; ++k) {
				postprocessed_quantities[q][internal_first_velocity_component+k] =
					uh[q][first_momentum_component+k] / uh[q][density_component];
			}
		}
		
		// compute pressure
		if (output_quantities & OutputQuantities::pressure) {
			postprocessed_quantities[q][internal_pressure_component] = pressure(uh[q]);
		}
		
		// store total energy per unit volume
		if (output_quantities & OutputQuantities::total_energy_volume) {
			postprocessed_quantities[q][internal_total_energy_volume_component] =
				uh[q][energy_component];
		}
		
		// compute total energy per unit mass
		if (output_quantities & OutputQuantities::total_energy_mass) {
			postprocessed_quantities[q][internal_total_energy_mass_component] =
				uh[q][energy_component] / uh[q][density_component];
		}
	}
}


template<int dim, int n_components>
std::vector< std::string >
DataPostprocessor<dim,n_components>::
get_names() const {
	return postprocessed_quantities_names;
}


template<int dim, int n_components>
std::vector< dealii::DataComponentInterpretation::DataComponentInterpretation >
DataPostprocessor<dim,n_components>::
get_data_component_interpretation() const {
	return dci;
}


template<int dim, int n_components>
dealii::UpdateFlags
DataPostprocessor<dim,n_components>::
get_needed_update_flags() const {
	return uflags;
}


template<int dim, int n_components>
double
DataPostprocessor<dim,n_components>::
kinetic_energy(const dealii::Vector<double> &uh) const {
	AssertThrow(uh[density_component] != 0., dealii::ExcDivideByZero());
	double kinetic_energy = 0;
	
	for (unsigned int k(0); k < dim; ++k) {
		kinetic_energy +=
			uh[first_momentum_component+k] * uh[first_momentum_component+k];
	}
	
	kinetic_energy /= (2. * uh[density_component]);
	
	return kinetic_energy;
}


template<int dim, int n_components>
double
DataPostprocessor<dim,n_components>::
pressure(const dealii::Vector<double> &uh) const {
	return (gamma_minus_one) * (uh[energy_component] - kinetic_energy(uh));
}


}} // namespace

#include "EulerEquationsDataPostprocessor.inst.in"

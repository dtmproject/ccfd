# README #

This README documents whatever steps are necessary to get the application
  DTM++/CCFD (Computational Compressible Fluid Dynamics)
up and running.

### What is this repository for? ###

* DTM++/CCFD: A frontend solver for the Euler Equation of Gas Dynamics.
* Prerelease Version 0.0.1

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Principial Author: Dipl.-Ing. Uwe Koecher (koecher@hsu-hamburg.de)

### License ###
Copyright (C) 2012-2015 by Uwe Koecher

This file is part of DTM++/CCFD (Computational Compressible Fluid Dynamics).

DTM++/CCFD is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

DTM++/CCFD is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.
 
You should have received a copy of the GNU Lesser General Public License
along with DTM++/CCFD. If not, see <http://www.gnu.org/licenses/>.
Please see the file
	./LICENSE
for details.
